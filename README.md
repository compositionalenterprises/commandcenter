# CommandCenter

CommandCenter is the 100% open source software billing platform built for managing software-as-a-service. Right now it can be used as:
  - Billing Platform to manage customer instances of OurCompose

## Development

To get your environment setup, follow the community setup guide for your operating system:
  1. If you're on Ubuntu, try the [Ubuntu development guide](docs/ubuntu.md).


## Setting Up CommandCenter

If you would like to setup a CommandCenter for production use, see the [***CommandCenter Install Guide***](docs/INSTALL.md).


## License
MIT

## Author Info

Don't hesitate to reach out: [OurCompose Support Team](mailto:support@ourcompose.com)

