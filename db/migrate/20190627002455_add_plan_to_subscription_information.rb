class AddPlanToSubscriptionInformation < ActiveRecord::Migration[5.2]
  def change
    change_table :subscription_informations do |t|
      t.string :plan_id
      t.string :plan_status
    end
  end
end
