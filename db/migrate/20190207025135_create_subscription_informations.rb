class CreateSubscriptionInformations < ActiveRecord::Migration[5.2]
  def change
    create_table :subscription_informations do |t|
      t.references :user
      t.string :stripe_subscription_id
      t.timestamps
    end
  end
end
