class CreatePlans < ActiveRecord::Migration[5.2]
  def change
    create_table :plans do |t|
      t.string :stripe_plan_id
      t.string :plan_status
      t.references :user
      t.timestamps
    end
  end
end
