class CreateServices < ActiveRecord::Migration[5.2]
  def change
    create_table :services do |t|
      t.string :name
      t.boolean :configured
      t.boolean :online
      t.string :status
      t.references :user
      t.timestamps
    end
  end
end
