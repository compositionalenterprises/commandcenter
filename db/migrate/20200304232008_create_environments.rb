class CreateEnvironments < ActiveRecord::Migration[6.0]
  def change
    create_table :environments do |t|
      t.string :gitlab_name
      t.string :gitlab_commit_created_at
      t.string :gitlab_commit_title
      t.string :gitlab_commit_authored_date

      t.timestamps
    end
  end
end
