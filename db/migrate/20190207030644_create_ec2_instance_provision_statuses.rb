class CreateEc2InstanceProvisionStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :ec2_instance_provision_statuses do |t|
      t.references :user
      t.integer :current_status_id
      t.string :current_status_comment
      t.timestamps
    end
  end
end
