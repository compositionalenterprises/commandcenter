class FixDomainName < ActiveRecord::Migration[5.2]
  def change
    rename_column :users, :wanted_domain, :domain
  end
end
