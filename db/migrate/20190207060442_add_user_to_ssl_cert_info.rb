class AddUserToSslCertInfo < ActiveRecord::Migration[5.2]
  def change
    add_reference :ssl_cert_infos, :user
  end
end
