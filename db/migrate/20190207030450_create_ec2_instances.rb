class CreateEc2Instances < ActiveRecord::Migration[5.2]
  def change
    create_table :ec2_instances do |t|
      t.references :user
      t.string :internal_ip
      t.string :instance_id
      t.string :private_dns_name
      t.timestamps
    end
  end
end
