class DroppingTables < ActiveRecord::Migration[6.0]
  def up
    drop_table :wcc_options
    drop_table :ssl_cert_infos
    drop_table :ec2_instances
    drop_table :ec2_instance_provision_statuses
  end

  def down
    fail ActiveRecord::IrreversibleMigration
  end
end
