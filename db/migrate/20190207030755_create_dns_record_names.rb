class CreateDnsRecordNames < ActiveRecord::Migration[5.2]
  def change
    create_table :dns_record_names do |t|
      t.references :user
      t.string :name
      t.string :change_id
      t.string :internal_external
      t.string :full_dns_name
      t.timestamps
    end
  end
end
