class CreateSslCertInfos < ActiveRecord::Migration[5.2]
  def change
    create_table :ssl_cert_infos do |t|
      t.references :dnsRecordName
      t.string :cert_domain_name
      t.string :cert_pem_value
      t.string :cert_key_file_name
      t.timestamps
    end
  end
end
