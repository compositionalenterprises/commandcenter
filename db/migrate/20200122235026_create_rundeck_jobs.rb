class CreateRundeckJobs < ActiveRecord::Migration[6.0]
  def change
    create_table :rundeck_jobs do |t|
      t.string :job_id
      t.string :job_name
      t.string :description
      t.references :user
      t.timestamps
    end
  end
end
