class CreateWccOptions < ActiveRecord::Migration[5.2]
  def change
    create_table :wcc_options do |t|
      t.string :option_name
      t.string :option_value
      t.timestamps
    end
  end
end
