class CreateServers < ActiveRecord::Migration[6.0]
  def change
    create_table :servers do |t|
      t.string :name
      t.boolean :online
      t.string :ip
      t.string :do_id
      t.string :floating_ip
      t.string :status
      t.string :outstanding

      t.references :user
      t.timestamps
    end
  end
end
