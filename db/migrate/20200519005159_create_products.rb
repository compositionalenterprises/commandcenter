class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.boolean :in_production
      t.boolean :coming_soon

      t.string :name
      t.string :image_link
      t.string :short

      t.text :html

      t.timestamps
    end
  end
end
