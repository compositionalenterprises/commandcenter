class AddDomainCheckedToUsers < ActiveRecord::Migration[5.2]
  def change
    change_table :users do |t|
      t.boolean :domain_valid
    end
  end
end
