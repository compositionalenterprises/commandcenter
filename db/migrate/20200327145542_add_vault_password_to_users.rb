class AddVaultPasswordToUsers < ActiveRecord::Migration[6.0]
  def change
    change_table :users do |t|
      t.string :encrypted_vault_password
    end
  end
end
