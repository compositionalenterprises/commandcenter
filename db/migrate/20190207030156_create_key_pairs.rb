class CreateKeyPairs < ActiveRecord::Migration[5.2]
  def change
    create_table :key_pairs do |t|
      t.references :user
      t.string :key_pair_name
      t.string :fingerprint
      t.string :material
      t.timestamps
    end
  end
end
