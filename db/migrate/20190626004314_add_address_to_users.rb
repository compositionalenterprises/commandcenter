class AddAddressToUsers < ActiveRecord::Migration[5.2]
  def change
    change_table :users do |t|
      t.string   :address
      t.string   :address2
      t.string   :city
      t.string   :zip_code
      t.string   :state
    end
  end
end
