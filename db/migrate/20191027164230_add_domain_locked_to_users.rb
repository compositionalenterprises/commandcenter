class AddDomainLockedToUsers < ActiveRecord::Migration[6.0]
  def change
    change_table :users do |t|
      t.boolean :domain_locked, :default => false
    end
  end
end
