class CreateCustomerInformations < ActiveRecord::Migration[5.2]
  def change
    create_table :customer_informations do |t|
      t.references :user
      t.string :stripe_customer_id
      t.timestamps
    end
  end
end
