class AddDefaultValueToServicesStatus < ActiveRecord::Migration[5.2]
  def change
    change_column :services, :status, :string, :default => "invalidAccount"
  end
end
