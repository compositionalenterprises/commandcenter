

if RundeckConfigurationOption.find_by(name: "CREATE_JOB_ID") == nil
    create_job_id = RundeckConfigurationOption.create(name: "CREATE_JOB_ID", value: "")
end

if RundeckConfigurationOption.find_by(name: "MIGRATE_JOB_ID") == nil
    migrate_job_id = RundeckConfigurationOption.create(name: "MIGRATE_JOB_ID", value: "")
end

if RundeckConfigurationOption.find_by(name: "DELETE_JOB_ID") == nil
    delete_job_id = RundeckConfigurationOption.create(name:"DELETE_JOB_ID", value: "")
end

if RundeckConfigurationOption.find_by(name: "AUTH_TOKEN_TO") == nil
    auth_token = RundeckConfigurationOption.create(name:"AUTH_TOKEN_TO", value: "")
end

if RundeckConfigurationOption.find_by(name: "AUTH_TOKEN_FROM") == nil
    auth_token = RundeckConfigurationOption.create(name:"AUTH_TOKEN_FROM", value: "")
end

if RundeckConfigurationOption.find_by(name: "BASE_URL") == nil
    webname = RundeckConfigurationOption.create(name:"BASEURL", value: "https://compositionalenterprises.ourcompose.com")
end

if RundeckConfigurationOption.find_by(name: "CREATE_ENVIRONMENT_ID") == nil
    create_environment_id = RundeckConfigurationOption.create(name:"CREATE_ENVIRONMENT_ID", value: "")
end

if RundeckConfigurationOption.find_by(name: "PROJECT_ID") == nil
    project_id = RundeckConfigurationOption.create(name:"PROJECT_ID", value: "")
end
