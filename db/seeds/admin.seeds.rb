
if ENV["CREATE_UPDATE"] == "CREATE"
    if !Admin.find_by(email: ENV["EMAIL"]).blank?
        exit 127
        puts "Admin Exists"
    else
        new_admin = Admin.create(email: ENV["EMAIL"], password: ENV["PASSWORD"], password_confirmation: ENV["PASSWORD"])

        if ENV["SEND_EMAIL_FLAG"] == "YES"
            AdminMailer.confirmation_instructions(new_admin, new_admin.confirmation_token, {password: ENV["PASSWORD"]}).deliver
        else
            new_admin.skip_confirmation!
            new_admin.confirm
        end
        new_admin.save!
    end
elsif ENV["CREATE_UPDATE"] == "UPDATE"
    admin = Admin.find_by(email: ENV["EMAIL"])

    if ENV["SEND_EMAIL_FLAG"] == "YES"
        admin.password = ENV["PASSWORD"]
        admin.password_confirmation = ENV["PASSWORD"]
        admin.save!
    else
        admin.password = ENV["PASSWORD"]
        admin.password_confirmation = ENV["PASSWORD"]
        admin.skip_reconfirmation!
        admin.save!
    end
else
    puts "Failure to recognize verb"
    puts "Exiting"
    exit 1
end