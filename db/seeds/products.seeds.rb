
# Seeds Products if they don't exist

if Product.find_by(name:"portal") == nil
    pr = Product.new
    pr.name = "portal"
    pr.in_production = true
    pr.short = "Portal is an application used to manage your instance."
    pr.html = ""
    pr.save!
end

if Product.find_by(name:"nextcloud") == nil
    pr = Product.new
    pr.name = "nextcloud"
    pr.in_production = true
    pr.short = "Nextcloud offers the industry-leading, on-premises content collaboration platform."
    pr.html = ""
    pr.save!
end

if Product.find_by(name:"bitwarden") == nil
    pr = Product.new
    pr.name = "bitwarden"
    pr.in_production = true
    pr.short = "Bitwarden is a free and open-source password management service that stores sensitive information such as website credentials in an encrypted vault."
    pr.html = ""
    pr.save!
end

if Product.find_by(name:"wordpress") == nil
    pr = Product.new
    pr.name = "wordpress"
    pr.in_production = true
    pr.short = "WordPress is open source software you can use to create a beautiful website, blog, or app."
    pr.html = ""
    pr.save!
end

if Product.find_by(name:"kanboard") == nil
    pr = Product.new
    pr.name = "kanboard"
    pr.in_production = true
    pr.short = "Kanboard is a free and open source Kanban project management software."
    pr.html = ""
    pr.save!
end

if Product.find_by(name:"rundeck") == nil
    pr = Product.new
    pr.name = "rundeck"
    pr.in_production = true
    pr.short = "Rundeck is open source Runbook Automation software."
    pr.html = ""
    pr.save!
end

if Product.find_by(name:"jekyll") == nil
    pr = Product.new
    pr.name = "jekyll"
    pr.in_production = true
    pr.short = "Jekyll is a simple, blog-aware, static site generator for personal, project, or organization sites."
    pr.html = ""
    pr.save!
end

if Product.find_by(name:"fireflyiii") == nil
    pr = Product.new
    pr.name = "firefly-III"
    pr.in_production = true
    pr.short = "Firefly III is a free and open source personal finance manager."
    pr.html = ""
    pr.save!
end

if Product.find_by(name:"bookstack") == nil
    pr = Product.new
    pr.name = "bookstack"
    pr.in_production = true
    pr.short = "BookStack is a simple, self-hosted, easy-to-use platform for organising and storing information."
    pr.html = ""
    pr.save!
end

if Product.find_by(name:"suitecrm") == nil
    pr = Product.new
    pr.name = "suitecrm"
    pr.in_production = true
    pr.short = "SuiteCRM is an open source Customer Relationship Management (CRM) software solution that provides a 360-degree view of your customers and business."
    pr.html = ""
    pr.save!
end


if Product.find_by(name:"akaunting") == nil
    pr = Product.new
    pr.name = "akaunting"
    pr.in_production = true
    pr.short = "Akaunting is a free, open-source, and online accounting software for small businesses and freelancers."
    pr.html = ""
    pr.save!
end

# Coming Soon
#
#if Product.find_by(name:"email server") == nil
#    pr = Product.new
#    pr.name = "email server"
#    pr.coming_soon = true
#    pr.short = "An email server built for you and custom domains."
#    pr.html = ""
#    pr.save!
#end
#
#if Product.find_by(name:"user management") == nil
#    pr = Product.new
#    pr.name = "user management"
#    pr.coming_soon = true
#    pr.short = "User management is a service deployed and built in to our team and enterprise editions making user management a piece of cake."
#    pr.html = ""
#    pr.save!
#end
#
#if Product.find_by(name:"bitcoin node") == nil
#    pr = Product.new
#    pr.name = "bitcoin node"
#    pr.coming_soon = true
#    pr.short = "A full node is a program that fully validates transactions and blocks."
#    pr.html = ""
#    pr.save!
#end
#
#if Product.find_by(name:"oauth server") == nil
#    pr = Product.new
#    pr.name = "oauth server"
#    pr.coming_soon = true
#    pr.short = "OAuth is a piece of software that implements network protocol flows which allow a client (piece of software) to act on behalf of a user."
#    pr.html = ""
#    pr.save!
#end
#
#if Product.find_by(name:"discourse") == nil
#    pr = Product.new
#    pr.name = "discourse"
#    pr.coming_soon = true
#    pr.short = "Discourse is an open source Internet forum and mailing list management software application."
#    pr.html = ""
#    pr.save!
#end
#
#if Product.find_by(name:"synapse") == nil
#    pr = Product.new
#    pr.name = "synapse"
#    pr.coming_soon = true
#    pr.short = "Synapse is the most widely installed Matrix homeserver implementation."
#    pr.html = ""
#    pr.save!
#end
#
#if Product.find_by(name:"phpmyldapadmin") == nil
#    pr = Product.new
#    pr.name = "phpmyldapadmin"
#    pr.coming_soon = true
#    pr.short = "phpLDAPadmin is a web app for administering Lightweight Directory Access Protocol servers."
#    pr.html = ""
#    pr.save!
#end
#
#if Product.find_by(name:"gittea") == nil
#    pr = Product.new
#    pr.name = "gittea"
#    pr.coming_soon = true
#    pr.short = "Gitea is an open-source forge software package for hosting software development version control using Git as well as other collaborative features like bug tracking, wikis and code review."
#    pr.html = ""
#    pr.save!
#end
#
#if Product.find_by(name:"openbazaar") == nil
#    pr = Product.new
#    pr.name = "openbazaar"
#    pr.coming_soon = true
#    pr.short = "OpenBazaar is an open source project developing a protocol for e-commerce transactions in a fully decentralized marketplace."
#    pr.html = ""
#    pr.save!
#end
#
#if Product.find_by(name:"nodebb") == nil
#    pr = Product.new
#    pr.name = "nodebb"
#    pr.coming_soon = true
#    pr.short = "NodeBB is a next generation forum software that's free and easy to use."
#    pr.html = ""
#    pr.save!
#end
#
#if Product.find_by(name:"wireguard vpn") == nil
#    pr = Product.new
#    pr.name = "wireguard vpn"
#    pr.coming_soon = true
#    pr.short = "WireGuard is a free and open-source software application and communication protocol that implements virtual private network techniques to create secure point-to-point connections in routed or bridged configurations."
#    pr.html = ""
#    pr.save!
#end
#
#if Product.find_by(name:"openvpn") == nil
#    pr = Product.new
#    pr.name = "openvpn"
#    pr.coming_soon = true
#    pr.short = "OpenVPN is open-source commercial software that implements virtual private network techniques to create secure point-to-point or site-to-site connections in routed or bridged configurations and remote access facilities."
#    pr.html = ""
#    pr.save!
#end
#
#if Product.find_by(name:"matomo") == nil
#    pr = Product.new
#    pr.name = "matomo"
#    pr.coming_soon = true
#    pr.short = "Matomo, formerly Piwik, is a free and open source web analytics application"
#    pr.html = ""
#    pr.save!
#end
#
#if Product.find_by(name:"redis") == nil
#    pr = Product.new
#    pr.name = "redis"
#    pr.coming_soon = true
#    pr.short = "Redis is an in-memory data structure project implementing a distributed, in-memory key-value database with optional durability."
#    pr.html = ""
#    pr.save!
#end
#
