Rails.configuration.stripe = {
    :publishable_key => Rails.application.credentials.stripe[:publishable_key],
    :secret_key => Rails.application.credentials.stripe[:secret_key]
}

# Note these are test API Keys
# Need to pull these keys from the environment...
#
Stripe.api_key = Rails.configuration.stripe[:secret_key]
