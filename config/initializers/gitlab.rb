Gitlab.configure do |config|
    config.endpoint       = 'https://gitlab.com/api/v4' 
    config.private_token = nil
    if !ENV['GITLAB_API_PRIVATE_TOKEN'].nil?
        config.private_token = ENV['GITLAB_API_PRIVATE_TOKEN']
    else
        config.private_token = Rails.application.credentials.gitlab[:key]
    end
    # Optional
    # config.user_agent   = 'Custom User Agent'          # user agent, default: 'Gitlab Ruby Gem [version]'
    # config.sudo         = 'user'                       # username for sudo mode, default: nil
end

