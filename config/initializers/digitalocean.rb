
require 'droplet_kit'
client = nil

if ENV['DO_API_KEY'] != nil
    client = DropletKit::Client.new(access_token: ENV['DO_API_KEY'])
else
    client = DropletKit::Client.new(access_token: Rails.application.credentials.do[:api_key])
end

