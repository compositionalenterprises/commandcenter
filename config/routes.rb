Rails.application.routes.draw do
  devise_for :admins, :skip => :registrations
  devise_for :users, controllers: {sessions: 'users/sessions', registrations: 'users/registrations', confirmations: 'users/confirmations'}

  authenticated :user do
    root to: "users#show", as: :authenticated_user
    resource :billing, controller: 'billing'
    namespace :billing do
      resource :customer, controller: 'customer'
      resources :charges
      resource :plans
    end
    #resources :services, path:'console'
    
    namespace :services do
      post 'deploy', to: 'deploy'
      get 'status', to: 'status'
    end

    resource :users, path:'account', only: [:show, :update]
    namespace :users, path:'account' do
      # Change this to something else...
      get 'applications', controller: 'applications', action: 'get_user_applications'
      post 'applications', controller: 'applications', action: 'select_applications'
      get 'settings', to: 'settings'
      match 'provision', to: 'provision', via: [:post] #needs to be a post request
      get 'provision_status', action: 'current_provision_status'
    end
  end

  authenticated :admin do
    devise_for :admins, only: [:registrations], controllers: { registrations: 'admins/registrations' }
    root to: "admins#show", as: :authenticated_admin
    namespace :admins do
      get 'user_statuses', to: 'users_statuses'
      get 'chart_data', to: 'chart_data', action:'chart_data'
      get 'validations', to: 'domain_validations', action:'domain_validations'
      post 'validate', to: 'validate_domain', action: 'validate_domain'
      post 'invalidate', to: 'invalidate_domain', action: 'invalidate_domain'
      get 'builds', to: 'build_tickets', action:'build_tickets'
      get 'decoms', to: 'decom_tickets', action:'decom_tickets'
      post 'mark_build_complete', to: 'mark_build_complete', action:'mark_build_complete'
      post 'mark_decom_complete', to: 'mark_decom_complete', action:'mark_decom_complete'

      get 'centerview', to:'centerview'
      namespace :centerview do
        get 'environments', to: 'environments'
        get 'subscriptions', to: 'subscriptions'
        get 'servers', to: 'servers'
        get 'domains', to: 'domains'
        get 'states', to: 'states'
      end

      resource :concierge, controller: "concierge"
      mount Blazer::Engine, at: "blazer"
    end
  end

  namespace :rundeck do
    post 'start', to: 'start', action: 'start'
    post 'complete', to: 'complete', action: 'complete'
    post 'fail', to: 'fail', action: 'fail'
    resource :options, only: [:show, :update]
    resource :datapoints, only: [:show] do
      get 'environment', action: 'environment_job_info'
      get 'instance', action: 'instance_job_info'
      get 'migration', action: 'migration_job_info'
    end
  end

  # unauthenticated :user do
  #   root "staticpages#index"
  # end

  root 'staticpages#index'

  get 'about', to: 'staticpages#about'
  get 'alternatives', to: 'staticpages#alternatives'
  get 'consulting', to: 'consultations#index'
  get 'contact', to: 'staticpages#contact'
  get 'home', to: 'staticpages#index'
  get 'pricing', to: 'staticpages#pricing'
  get 'privacy', to: 'staticpages#privacy'
  get 'products', to: 'products#list'
  get 'terms', to: 'staticpages#terms'

  post 'contact', to: 'staticpages#create_contact', as: 'contact_informations'
  namespace :products, path: "products" do
    get 'all', to: 'all'
  end
  resources :products, except: [:index]

  scope 'cases', module: "staticpages/cases", as: "cases" do
    get 'index', to: 'index'
    get 'students', to: 'students'
    get 'developers', to: 'developers'
    get 'projects', to: 'projects'
    get 'owners', to: 'owners'
    get 'leads', to: 'leads'
  end


  # resource :support
  # External Sites
  get "/docs" => redirect("https://compositionalenterprises.ourcompose.com/bookstack/books"), :as => :docs
  get "/gitlab" => redirect("https://gitlab.com/compositionalenterprises"), :as => :gitlab
  get "/forum" => redirect("https://reddit.com/r/ourcompose"), :as => :forum
  get "/podcast" => redirect("https://ourcomposecast.com"), :as => :podcast
  get "/roadmap" => redirect("https://compositionalenterprises.ourcompose.com/kanboard/public/board/245d06ce519a4deba6131ea58a65afa9efc0e6de584e04b08a81c4115322"), :as => :roadmap
end
