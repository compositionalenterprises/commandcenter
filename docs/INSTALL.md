# How Do I Run CommandCenter?

> The only officially supported installs of CommandCenter are [Docker](https://www.docker.io) based. You must have SSH access to a 64-bit Linux server **with Docker support**. We regret that we cannot support any other methods of installation including cpanel, plesk, webmin, etc.

## Set Up CommandCenter

Pull down the latest docker image of CommandCenter: 

```sh
docker pull compositionalenterprises/commandcenter:latest
```

### RAILS_RELATIVE_URL_ROOT

This application is configured to run at the base address of `localhost:3000/commandcenter`. The production assets are configured for this address, but in development they use the `./bin/webpack-dev-server` `localhost:3035` address. The proxy 

If you need to change this application to run at the root url, you will need to change the `config.ru` file to look like below:

```ruby
# This file is used by Rack-based servers to start the application.

require_relative 'config/environment'

run Rails.application

```

### Set the proper Environment Variables when deploying the docker container:
  - DB_HOST: database
  - DB_NAME: 'commandcenter'
  - DB_USER: 'commandcenter'
  - DB_PASS: "{{ compositional_commandcenter_backend_password }}"
  - RAILS_RELATIVE_URL_ROOT: '/commandcenter'
  - ADMIN_EMAIL: "{{ environment_admin }}@{{ environment_domain }}"
  - ADMIN_PASSWORD: "{{ compositional_commandcenter_admin_password }}"

Please note `ADMIN_EMAIL` and `ADMIN_PASSWORD` are only needed for the initial deploy.
Use the `-e` flag to set these environment variables when creating the container.

### Pass production keys to the container in volumes when running in production
  - `-v /location-on-host/production.key:/app/config/credentials/production.key`
  - `-v /location-on-host/production.yml.enc:/app/config/credentials/production.yml.enc`

By default, sample keys will be passed to the application, but the applicaion **will fail** to run critical components such as `email` and `billing`.

A sample set of credentials in this repo can be viewed by using the commands below (with the proper ruby/rails environment on the host OS):
```sh
EDITOR=vim rails credentials:edit -e sample
```

An example of creating and running a production container from the commandcenter image with a local mysql database instance named `commandcenter` with user `cc` and password `testpassword` would look like the following from the command line:
```sh
docker run \
  -p 3000:3000 \
  -v "$(pwd)/config/credentials/production.key:/app/config/credentials/production.key" \
  -v "$(pwd)/config/credentials/production.yml.enc:/app/config/credentials/production.yml.enc" \
  -e DB_HOST=host.docker.internal \
  -e DB_NAME=commandcenter \
  -e DB_USER=cc \
  -e DB_PASS=testpassword \
  --name commandcenter \
  compositionalenterprises/commandcenter:latest
```

### Adding other Admin Users

Running a seed to add admins will also add other admin users if they do not exist.
The database needs to be online for this script to work properly.
```sh
docker run --rm commandcenter bundle exec rake db:seed:single SEED=admin EMAIL="moorew1997@gmail.com" PASSWORD="P4ssword"
```