# CommandCenter Jobs

CommandCenter runs jobs to ensure the validity of customer information and server integrity.
These jobs run against external API's such as Stripe as well as DigitalOcean.

## Instance Mapper

The instance mapper job takes everything in the DO environment and maps it to a customer within the CommandCenter database. 
If the server is unable to map to a user, then the server is marked as `outstanding`. 

