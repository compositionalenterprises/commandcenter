# Ubuntu Development Guide

If at any point you get stuck, please don't hesitate to reach out to the core team for help.

### Install Dependencies
  - Ruby: ruby 2.6.3p62 (2019-04-16 revision 67580) [x86_64-darwin18]
  - Rails: 6.0.0
  - Webpacker: 4.0.7
  - Node: v12.4.0
  - Yarn: 1.16.0

  - @rails/webpacker:
  - └── @rails/webpacker@4.0.7 

  - Is bin/webpack present?: true
  - Is bin/webpack-dev-server present?: true
  - Is bin/yarn present?: true

### Clone CommandCenter

Clone the CommandCenter repository (or your fork) in `~/CommandCenter` folder:

`git clone git@gitlab.com:compositionalenterprises/commandcenter.git ~/CommandCenter`

## Services

Commandcenter requires a database backend to be running as well as a react frontend server to be running.

### Setup Database

From the `CommandCenter` repository run `docker-compose up db` and this will initialize an empty database.

### Webpack Dev Server

From the `CommandCenter` repository run `./bin/webpack-dev-server` and this will start the frontend server.

