## Docker?

If you're deploying to docker, this guide is for you.

### Compile Assets Correctly

When pushing the local image, ensure the asset pipeline is correct

Use:
```sh
./bin/webpack
```

Over:
```sh
bundle exec rails webpacker:compile
```

Rails's webpacker compile does something funky and the `manifest.json` doesn't match the hashes of the actual values. So compiling ends up being fucked.

## PUUUUUSH

From there, build the image from your local machine where the packs exist. Make sure to remove the existing image if its already there.

```sh
docker image rm compositionalenterprises/commandcenter:mvp

docker build -t compositionalenterprises/commandcenter:mvp .

docker push compositionalenterprises/commandcenter:mvp
```

This will push the image to the docker repo.

## CICD Errors

Application isn't compiling anything in CICD. Need to look into using `./bin/webpack` in the container to see if it creates the assets.
Running `bin/webpack` on local machine has been creating the assets and proper `manifest.json` file.

```sh
./bin/webpack
```
