# Manjaro

```
$ pamac install rvm docker docker-compose mariadb
$ systemctl -aG $USER docker
$ systemctl enable docker
$ systemctl reboot
$ source /usr/share/rvm/scripts/rvm
$ rvm install 2.7.0
$ bundle install
$ yarn install
$ export DB_PASS=testpassword
$ export DB_NAME=webcommandcenter_dev
$ export DB_USER=root
$ docker-compose up -d --build
$ docker-compose down website
$ ./bin/webpack-server-dev
$ bundle exec rails s
```

# TODO
- Sample keys
- Fixed sign-up emails
