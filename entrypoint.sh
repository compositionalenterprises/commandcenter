#!/bin/sh

export RAILS_ENV=production

if [ ! -f ./config/credentials/production.key ]; then
    mv ./config/credentials/production.yml.enc ./config/credentials/production.yml.bak
    mv ./config/credentials/sample.key ./config/credentials/production.key
    mv ./config/credentials/sample.yml.enc ./config/credentials/production.yml.enc
fi

bundle exec rake db:exists && rake db:migrate || rake db:setup

if [[ -z "${ADMIN_EMAIL}" ]] || [[ -z "${ADMIN_PASSWORD}" ]]; then 
    echo "Admin email or password not set, not seeding!"
else
    echo "Service Deprecated, no admins added!"
    echo "Continuing on to start Server!"
    echo "To Add an Admin use rake_admin.sh script found in /app/bin/seeds"
    echo "With Container running, use a post container script such as docker exec with environment variables"
fi

bundle exec rake db:seed:single SEED=rundeckconfigurationoption
bundle exec rake db:seed:single SEED=products

exec "$@"

