FactoryBot.define do
  factory :contact_information do
    
  end

  factory :product do
    
  end

  factory :application do
    
  end

    factory :server do
    end

    factory :environment do
    end

    factory :rundeck_configuration_option do 
    end

    factory :rundeck do  
    end

    factory :rundeck_job do
        job_id {"1"}
        job_name {"create_environment"}
        description {"started"}
        user_id {1}
    end

    factory :user do
        firstname { "John" }
        lastname  { "Doe" }
        email { "john_doe5@gmail.com" }
        password { "P4ssword1" }
        password_confirmation { "P4ssword1" }
        domain {"facebook.com"}
        domain_valid {false}
        after(:create) { |user| user.confirm }
        id { 1 }
    end

    factory :admin do
        email { "testytest@compositional.enterprises" }
        password { "P4ssword1" }
        password_confirmation { "P4ssword1" }
        after(:create) { |user| user.confirm }
        id { 1 }
    end

    factory :service do
        status { "invalidAccount" }
        user_id { 1 }
    end

    factory :subscriptionInformation do
        # Need to pull test data from stripe
        stripe_subscription_id {"sub_Fv6spDDa6CYnoX"}
        id {1}
        user_id { 1 }
    end

    factory :customerInformation do
        # Need to pull test data from stripe
        user_id { 1 }
        stripe_customer_id {"cus_Fv6rI9IYcvkqNN"}
        id { 1 }
    end

    factory :plan do
        # Need to pull test data from stripe
        stripe_plan_id { "plan_FHmI2YtEAE2AND" }
        plan_status { "valid" }
        user_id { 1 }
        id { 1 }
    end

end