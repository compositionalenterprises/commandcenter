require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the RundeckHelper. For example:
#
# describe RundeckHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe RundeckHelper, type: :helper do

  describe "start deploy" do
    it "starts the deploy process" do
      user = build(:user)
      expect(helper.start_deploy(user.domain, user.vault_password).to be_a(Mime::JSON))
    end
  end

  describe "create environment" do
    it "should create an environment" do
      user = build(:user)
      expect(helper.create_environment(user.domain, user.vault_password).to be_a(Mime::JSON))
    end
  end

  describe "create instance" do
    it "should create an instance" do
      user = build(:user)
      expect(helper.create_instance(user.domain, user.vault_password).to be_a(Mime::JSON))
    end

    it "should test the instance helper" do
      user = build(:user)
      expect(helper.create_instance_helper(user.domain, user.vault_password).to be_a(Mime::JSON))
    end
  end

end
