require 'rails_helper'
require 'stripe_mock'

RSpec.describe "StripeFlows", type: :request do
  # https://github.com/rebelidealist/stripe-ruby-mock
  let(:stripe_helper) { StripeMock.create_test_helper }
  let(:stripeTestUser) { Hash.new }
  before do 
    StripeMock.start
  end
  # before { MyStripe.start } 
  after { StripeMock.stop }
  # after { MyStripe.teardown }

  describe "Successfuly transform a user into a paying customer" do

    it "should create customer information for the user" do
      # stripeTestUser.stripe_customer_id = user.customerInformation.stripe_customer_id
    end
    it "should create a plan for the user" do
      # stripeTestUser.stripe_plan_id = user.plan.stripe_plan_id
    end
    it "should create a valid subscription for the user" do
    end
    it "should successfully unsubscribe a user from the platform" do
      #   # user.set_customer_information
      #   user.create_valid_subscription
      #   deleted_sub = user.cancel_account_subscription
      #   expect(deleted_sub[:status]).to eq("success")
      #   expect(deleted_sub[:message]).to eq("User has successfully been unsubscribed!")
    end
  end
end
