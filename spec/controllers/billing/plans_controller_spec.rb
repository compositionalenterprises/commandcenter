require 'rails_helper'

RSpec.describe Billing::PlansController, type: :controller do
    login_user

    it "should create a plan for the user" do
        # create
    end

    it "should fail to create a plan because the user already has one" do
        # create
        # create again & fail
    end

    it "should remove the user's plan" do
        # create & destroy a plan
    end

    it "should fail to remove a user's plan because the user doesn't have one" do
        # Fail on destroy and make sure it returns a pretty message!
    end



end
