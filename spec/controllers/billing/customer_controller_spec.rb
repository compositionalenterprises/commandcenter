require 'rails_helper'
require 'stripe_mock'

RSpec.describe Billing::CustomerController, type: :controller do
    # https://github.com/rebelidealist/stripe-ruby-mock
    login_user
    let(:stripe_helper) { StripeMock.create_test_helper }
    before { StripeMock.start }
    after { StripeMock.stop }

    it "should create a customer from a user" do
        post :create, params: {:stripeToken => stripe_helper.generate_card_token}
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)['status']).to eq("success")
        expect(JSON.parse(response.body)['message']).to eq("Created Card Information")
    end

    it "should return the user's subscription" do
        create(:customerInformation)
        create(:subscriptionInformation)
        get :show
        expect(JSON.parse(response.body)['status']).to eq("success")
        expect(JSON.parse(response.body)['subscriptions']["stripe_subscription_id"]).to eq("test_sub_id")
    end

    it "should return something showing the user's subscription is invalid or doesnt exist" do
        get :show
        expect(JSON.parse(response.body)['status']).to eq("error")
        expect(JSON.parse(response.body)['subscriptions']).to eq(nil)
    end

    it "should fail to create a new subscription because the user doesn't exist in stripe" do
        create(:customerInformation)
        create(:subscriptionInformation)
        post :create, params: {:stripeToken => stripe_helper.generate_card_token}
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)['status']).to eq("error")
        expect(JSON.parse(response.body)['message']).to eq("(Status 404) No such customer: test_cust_id")
    end

    it "should fail due to a card error" do
        create(:customerInformation)
        create(:subscriptionInformation)
        post :create, params: {:stripeToken => stripe_helper.generate_card_token({card: { number:"4000000000000002", exp_month: 10, exp_year: 2020,cvc: '314',}})}
        expect(JSON.parse(response.body)['status']).to eq("error")
        expect(JSON.parse(response.body)['message']).to eq("(Status 404) No such customer: test_cust_id")
    end



end
