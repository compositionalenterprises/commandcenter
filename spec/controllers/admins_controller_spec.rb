require 'rails_helper'

RSpec.describe AdminsController, type: :controller do
    login_admin
    let(:stripe_helper) { StripeMock.create_test_helper }
    before { StripeMock.start }
    after { StripeMock.stop }

    it "should create a subscription for a user" do
        create(:user)
        create(:customerInformation)
        create(:plan)
        post :mark_build_complete, params: { user_id: 1}
        expect(response).to have_http_status(200)
        # expect(JSON.parse(response.body)['status']).to eq("success")
        # expect(JSON.parse(response.body)['message']).to eq("You have successfully been subscribed.")
    end
    
    it "should remove an existing subscription for a user" do
        create(:user)
        create(:customerInformation)
        post :mark_decom_complete, params: { user_id: 1}
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)['status']).to eq("success")
        expect(JSON.parse(response.body)['message']).to eq("Account has successfully been removed!")
    end
end
