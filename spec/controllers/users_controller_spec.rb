require 'rails_helper'

RSpec.describe UsersController, type: :controller do
    login_user

    it "should have a current_user" do
        # note the fact that you should remove the "validate_session" parameter if this was a scaffold-generated controller
        expect(subject.current_user).to_not eq(nil)
    end

    it "should show that a user has completed user_info" do
        get :show, {}
        expect(response).to have_http_status(200)
    end

end
