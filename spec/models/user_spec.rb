require 'rails_helper'
require 'stripe_mock'

RSpec.describe User, type: :model do
  let(:stripe_helper) { StripeMock.create_test_helper }
  before { StripeMock.start }
  after { StripeMock.stop }

  it "should check the user's name" do
    user = create(:user)
    expect(user.firstname).to eq("John")
  end

  it "should check if the user needs their domain validated" do
    user = create(:user)
    expect(user.needs_domain_validated?).to eq(false)
  end

  it "should return nil for the deployment_status" do
    user = create(:user)
    expect(user.deployment_status).to eq("invalidAccount")
  end

  it "should return false after checking the domain validity" do
    user = create(:user)
    expect(user.has_valid_domain?).to eq(false)
  end

  it "should return true after checking the domain validity" do
    user = create(:user)
    user.update(:domain_valid => true)
    expect(user.has_valid_domain?).to eq(true)
  end

  # create_valid_subscription
  # Test internal and 3rd party errors
  # 

  it "should fail because the user doesn't have customer information" do
    user = create(:user)
    valid_sub = user.create_valid_subscription
    expect(valid_sub[:status]).to eq("error")
  end

  # Erroring Out big time.. :(
  # Need to look into 
  # May test this from the controller side
  # it "should create a valid subscription for a user" do
  #   user = create(:user)
  #   # user.set_customer_information
  #   customer = Stripe::Customer.create({
  #     email: user.email,
  #     source: stripe_helper.generate_card_token
  #   })
  #   user.create_customerInformation(stripe_customer_id: customer['id'])
  #   user.create_plan(:stripe_plan_id => "plan_FHmI2YtEAE2AND",  :plan_status => "valid")
  #   StripeMock.create_subscription_items
  #   valid_sub = user.create_valid_subscription
  #   expect(valid_sub[:status]).to eq("error")
  #   expect(valid_sub[:message]).to eq("You have successfully been subscribed.")
  # end

  it "should throw an error due to no network connection and return that error" do
  end

  it "should return an error due to no user plan" do
    user = create(:user)
    invalid_sub = user.create_valid_subscription
    expect(invalid_sub[:status]).to eq("error")
  end

  it "should return an error because the user doesn't have a stripe account" do
  end


  # cancel_account_subscription
  # Test both internal and external errors for potential bugs
  #

  # Erroring Out also big time... :(
  # it "should cancel an existing account subscription returning success with the user unsubscription" do
  #   user = create(:user)
  #   # user.set_customer_information
  #   user.create_valid_subscription
  #   deleted_sub = user.cancel_account_subscription
  #   expect(deleted_sub[:status]).to eq("success")
  #   expect(deleted_sub[:message]).to eq("User has successfully been unsubscribed!")
  # end

  it "should attempt to cancel a subscription for a user that does not have a subscription" do
    user = create(:user)
    user.create_valid_subscription
    deleted_sub = user.cancel_account_subscription
    expect(deleted_sub[:status]).to eq("success")
    expect(deleted_sub[:message]).to eq("User does not have subscriptions!")
  end


  # remove_current_plan 
  # Test to remove the current plan when decommissioning a server/user
  # 

  it "should return success because the user does not have a plan or has already been removed" do
    user = create(:user)
    returned_hash = user.remove_current_plan
    expect(returned_hash[:status]).to eq("success")
    expect(returned_hash[:message]).to eq("Plan has been removed! Check this user!")
  end

  it "should remove the current plan without error and return success" do
    user = create(:user)
    # user.set_plan_information
    returned_hash = user.remove_current_plan
    expect(returned_hash[:status]).to eq("success")
  end

end
