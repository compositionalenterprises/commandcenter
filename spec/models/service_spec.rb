require 'rails_helper'

RSpec.describe Service, type: :model do

  it "should check all a service states to confirm they exist" do 
    expect(Service::STATES.length).to eq(7)
  end

  it "should return a user's most recent service" do
    user = create(:user)
    most_recent_service = create(:service)
    expect(user.deployment_status).to eq("invalidAccount")
  end

  it "should set a user's services to invalid" do
    user = create(:user)
    dep_stat = user.set_services_invalid
    expect(dep_stat[:status]).to eq("invalidAccount")
  end

  it "should update a user's deployment status based on customer, plan and service information" do
  end

  it "should update a users depoyment status to deployed" do
    user = create(:user)
    dep_stat = user.update_deployment_status_deployed
    expect(dep_stat[:status]).to eq("deployComplete")
  end

  it "should update a user's deplyoment status to pending decommission" do
    user = create(:user)
    dep_stat = user.update_deployment_status_pending_decom
    expect(dep_stat[:status]).to eq("pendingUnsubscribe")
  end

  it "should update a user's deployment status to decommissioned" do
    user = create(:user)
    dep_stat = user.update_deployment_status_decommed
    expect(dep_stat[:status]).to eq("unsubscribed")
  end

  it "should update a user's status to pendingDeploy" do
    user = create(:user)
    dep_stat = user.update_deployment_status_domain
    expect(dep_stat[:status]).to eq("pendingDeploy")
  end

end
