# Preview all emails at http://localhost:3000/rails/mailers/concierge
class ConciergePreview < ActionMailer::Preview

    def new_signup
        ConciergeMailer.new_signup(User.first, "password1")
    end
    
end
