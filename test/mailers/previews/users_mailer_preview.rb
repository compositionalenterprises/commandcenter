class UsersMailerPreview < ActionMailer::Preview
    def welcome_email
        UsersMailer.with(email: User.first.email, token: "test", resource: User.first).confirmation_instructions
    end
    def email_changed
        #UsersMailer.with(email: User.first.email, token: "test", resource: User.first).confirmation_instructions
    end
    def password_change
        #UsersMailer.with(email: User.first.email, token: "test", resource: User.first).confirmation_instructions
    end
    def reset_password_instructions
        #UsersMailer.with(email: User.first.email, token: "test", resource: User.first).confirmation_instructions
    end
    def unlock_instructions
        #UsersMailer.with(email: User.first.email, token: "test", resource: User.first).confirmation_instructions
    end

    def contact_mailer
        @contact = {}
        ContactMailer.with(contact: @contact).contact_received
    end

    def instance_complete
        ContactMailer.with(user: User.first).instance_complete
    end
end