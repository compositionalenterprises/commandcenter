class UsersController < ApplicationController
    before_action :authenticate_user!
    before_action :validate_user_account_information, except: [:show, :update]

    # Look into moving this to seperate methods and routes
    def show
        selectedPlans = []
        if !current_user.plan.blank?
            selectedPlans = [reverseLookup(current_user.plan.stripe_plan_id)] # current_user.subscriptionInformation.plans
        end
        @userInfo = {
            hasValidAccount: current_user.has_valid_account?,
            hasValidDomain: current_user.has_valid_domain?,
            hasValidPayment: !current_user.customerInformation.blank?,
            hasValidPlan: !current_user.plan.blank?,
            hasValidApplications: current_user.has_valid_applications_selected?,
            selectedPlans: selectedPlans,
            personalInformation: {
                first_name: current_user.firstname,
                last_name: current_user.lastname,
                address: current_user.address,
                address2: current_user.address2,
                city: current_user.city,
                zip_code: current_user.zip_code,
                state: current_user.state,
                domain: current_user.domain,
                domain_locked: current_user.domain_locked
            },
            deploymentStatus: current_user.deployment_status,
        }
        @stripe_api_key = { 
            stripe_pub_key: Rails.application.credentials.stripe[:publishable_key]
        }
    end

    def update
        # This needs to save to data to database properly
        @user = current_user
        if @user.update(user_params)
            render json: {
                status: "success",
                message: "Successfully updated your account!"
            }
        else
            render json: {
                status: "error",
                errors: @user.errors
            }
        end
    end

    private

    def user_params
        params.require(:user).permit(:firstname, :lastname, :zip_code, :address, :address2, :city, :domain)
    end

    def validate_user_account_information
        if !current_user.has_valid_account?
            redirect_to users_path, flash: { message: "Please complete all of the items below!"}
        end
    end

    def reverseLookup(keyy)
        Plan::PLANS.key(keyy)
    end
end
