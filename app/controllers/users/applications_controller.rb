class Users::ApplicationsController < ApplicationController
    before_action :authenticate_user!

    def get_user_applications
        # GET /users/applications
        deployip = current_user.has_services_deployed? || current_user.deploy_in_progress?
        if !(current_user.applications.count == 0)
            render json: {"userApplicationsSelected": true, "applications": Application.currently_supported_applications, "currentlySelectedApplications": current_user.applications.map { |f| f.name }, "userServicesDeployed": deployip}
        else    
            render json: {"userApplicationsSelected": false, "applications": Application.currently_supported_applications, "currentlySelectedApplications": [], "userServicesDeployed": deployip }
        end
    end

    def select_applications
        # POST /users/applications
        current_user.applications.destroy_all
        applications.each do |f|
            Application.create(name: f, user_id: current_user.id)
        end
        render json: {"status": "success"}
    end

    private

    def applications
        params.require(:applications)
    end

end
