class ProductsController < ApplicationController
    before_action :authenticate_admin!, except: [:show, :list]

    def list
        @products_in_production = Product.where(in_production: true)
        @products_coming_soon = Product.where(coming_soon: true)
    end

    def all
        @all = Product.all
    end

    def show
        @product = Product.friendly.find(name_slugged)
    end

    def new
        @product = Product.new
    end

    def create
        Product.create(product_params)
        redirect_to products_url
    end

    def edit
        @product = Product.friendly.find(params[:id])
    end

    def update
        @product = Product.friendly.find(params[:id])
        @product.update!(product_params)
        redirect_to products_url
    end

    def destroy
        @product = Product.friendly.find(params[:id])
        @product.destroy
        redirect_to products_all_path
    end

    private

    def product_params
        params.require(:product).permit(:name, :short, :html, :in_production, :coming_soon)
    end

    def name_slugged
        params[:id].gsub("-", "")
    end
end
