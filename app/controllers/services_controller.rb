class ServicesController < ApplicationController
    # See app/controllers/rundeck_controller.rb for closing of loop
    # See lib/rundeck.rb for Rundeck Module; may use model to make these calls.

    before_action :authenticate_user!
    before_action :validate_user_account_information, except: [:deploy]

    def show
        render json:{
            configured: current_user.services.last.status,
        }
    end

    def deploy
        if validate_password_matches(deploy_params['password_confirmation'])
            if !current_user.services.blank?
                if current_user.services.last.status == "deployInProgress"
                    render json: {
                        status: "success",
                        message: current_user.services.order("created_at").last.status
                    }
                elsif current_user.services.last.status == "pendingDeploy"
                    # Call to the Rundeck API
                    # Potential for laggage..

                    services = nil
                    if !current_user.has_valid_applications_selected?
                        services = "wordpress,nextcloud,bitwarden"
                    else
                        services = current_user.current_applications_s[0..-2]
                    end
                    current_user.services.create(:status => "deployInProgress")
                    SignupAlertJob.perform_later(current_user.domain, "signed up with services #{services.to_s}")
                    StartDeployJob.perform_later(current_user.domain, current_user.encrypted_vault_password, services, deploy_params['password_confirmation']) #, caught_password
                    render json: {
                        status: "success",
                        message: "services are currently being built!"
                    }
                end
            else
                # Add Admin error here
                # Raise error of some kind to inform admins..
                render json: {
                    status: "error",
                    message: "Looks Like there is an error with your account! We have informed our admins, but please reach out if you have not heard from us in 3 days."
                }
            end
        else
            render json: {
                status: "error",
                message: "wrong-password"
            }
        end
    end

    def status
        render json: {
            status: "notice",
            message: current_user.deployment_status
        }
    end

    private

    def validate_user_account_information
        if !current_user.has_valid_account?
            return false
        else
            return true
        end
    end

    def validate_password_matches(password_confirmation)
        user = User.find_for_authentication(:email => current_user.email)
        return user.valid_password?(password_confirmation)
    end

    def deploy_params
        params.permit(:password_confirmation)
    end
end
