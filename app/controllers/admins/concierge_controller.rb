class Admins::ConciergeController < ApplicationController
    def show

        @available_applications = Application.currently_supported_applications
        @stripe_api_key = { 
            stripe_pub_key: Rails.application.credentials.stripe[:publishable_key]
        }
    end

    def create
        if params[:'customer-email'].blank? || !valid_domain? || params[:'token-id'].blank? || params[:'plan'].blank?
            redirect_to show, flash: { error: "Please ensure all data is filled out and correct."} and return
        end
        random_pw = SecureRandom.hex(6)

        # need to catch the error
        user = Admins::ConciergeHelper.create_user(params[:'customer-email'], params[:'customer-domain'], random_pw, applications)
        
        if user.nil? 
            user = User.find_by(email: params[:'customer-email'])
        end

        if user.nil?
            redirect_to show, flash: { error: "Failed to create user, please try again and also ensure the user does not already exist."} and return false
        end

        # need to catch the error
        customer = Admins::ConciergeHelper.create_stripe_customer(user, params[:'token-id'])
        if customer.nil?
            redirect_to show, flash: { error: "Failed to create Stripe account for the user."} and return false
        end

        a = user.create_plan(:stripe_plan_id => convert_plan_parameters(params[:'plan']))
        if a.nil?
            redirect_to show, flash: { error: "Failed to link the user to the Stripe plan."} and return false
        end
        user.update_deployment_status_domain
        services = nil
        if !user.has_valid_applications_selected?
            services = "wordpress,nextcloud,bitwarden"
        else
            services = user.current_applications_s[0..-2]
        end
        user.services.create(:status => "deployInProgress")
        StartDeployJob.perform_later(params[:'customer-domain'], user.encrypted_vault_password, services, random_pw)
        begin
            ConciergeMailer.with(user: user, password: random_pw).new_signup.deliver_now
        rescue SocketError => e
            puts "ERROR Sending Mail, Check Mailer Settings"
            redirect_to show, flash: { notice: "Successfully deployed new instance with Concierge!" } and return true
        end
        # need to return something....
        redirect_to show, flash: { notice: "Successfully deployed new instance with Concierge!" } and return true
    end

    protected

    def applications
        applications = []
        params.each do |k,v|
            if k.to_s.start_with?("application-")
                applications.append(v.to_s)
            end
        end
        if applications = []
            return ['kanboard','nextcloud','bitwarden']
        end
        return applications
    end

    def concierge_params
        params.require(:customer-email, :customer-domain, :token-id, :plan)
    end

    def convert_plan_parameters(plan_params)
        case plan_params
        when "personal"
            return Plan::PLANS[:personal_plan]
        when "personal-plus"
            return Plan::PLANS[:personal_plus_plan]
        else
            raise InvalidPlanException.new("Invalid Plan"), "You have selected an invalid plan!"
        end
    rescue InvalidPlanException => e
        return e
    end

    def valid_domain?
        if /^[a-zA-Z0-9]{3,24}.ourcompose.com$/.match(params[:'customer-domain']) == nil
            return false
        else
            return true
        end
    end

end

class InvalidPlanException < StandardError
end
