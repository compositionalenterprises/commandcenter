class Admins::CenterviewController < ApplicationController
    before_action :authenticate_admin!

    def subscriptions
        return_json = {}
        return_json['subscriptions'] = SubscriptionInformation.valid_plans
        render json: return_json
    end
    
    def environments
        return_json = {}
        return_json['environments'] = Environment.get_environments
        render json: return_json
    end

    def servers
        return_json = {}
        return_json['servers'] = Server.current_servers_name
        render json: return_json
    end

    def domains
        return_json = {}
        return_json['domains'] = User.all_domains
        render json: return_json
    end

    def states
        return_json = {}
        return_json['servers_with_offline_or_no_payment'] = Server.offline_or_no_payment
        return_json['matched_users_with_valid_payment'] = Server.matched_servers_to_users[:matched_users_with_valid_payment]
        return_json['matched_users_invalid_payment'] = Server.matched_servers_to_users[:matched_users_invalid_payment]
        return_json['misnamed_droplets'] = Server.matched_servers_to_users[:misnamed_droplets]
        return_json['subscription_no_environment'] = Environment.subscription_without_environment
        return_json['environment_no_instance'] = Environment.subscription_with_env_no_instance
        render json: return_json
    end
end
