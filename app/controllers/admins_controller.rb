class AdminsController < ApplicationController
    before_action :authenticate_admin!

    def show
        # @domainValidationsCount = User.where.not(domain: nil).where.not(domain: "").where(domain_valid: [false, nil]).count
        @domainValidationsCount = User.joins(:customerInformation).joins(:plan).where.not(domain: nil).where.not(domain: "").where(domain_valid: [false, nil]).count
        @buildTicketsCount = 0
        User.most_recent_service_status.each { |f| if f.services.last.status == "deployInProgress" then @buildTicketsCount+=1 end }
        @decomTicketsCount = 0
        User.most_recent_service_status.each { |f| if f.services.order('created_at desc').limit(1).first.status == "pendingUnsubscribe" then @decomTicketsCount+=1 end }        
        @supportTicketsCount = 0
        @userCount = User.all.count
        @usersWithValidDomain = User.joins(:customerInformation).joins(:plan).where(domain_valid: [false, nil]).count
        @decomTickets = []
        User.most_recent_service_status.each { |f| if f.services.order('created_at desc').limit(1).first.status == "pendingUnsubscribe" then @decomTickets.push(f) end }
    end

    def chart_data
        # Needs to be updated because now this is being automated
        return_json = {}
        temp_array = []
        User.all.each do |f|
            temp_array.push(f.services.last)
        end
        temp_array.each do |f|
          Service::STATES.each do |state|
            if return_json[state] == nil
              return_json[state] = 0
            end
            if !f.nil?
              if f.status == state
                return_json[state] += 1
              end
            end
          end
        end
        return_json['pendingValidDomain'] = Service.pendingValidDomain_list.count
        render json: return_json
    end

    def user_statuses
        users = []
        u3 = {}
        User.all.each do |f|
            to_add = {user: f.email, status: f.services.last.status}
            users.append(to_add)
            if u3[f.services.last.status].blank?
                u3[f.services.last.status] = []
                u3[f.services.last.status].append(f.email)
            else
                u3[f.services.last.status].append(f.email)
            end
        end
        @u2 = users.sort_by {|hsh| hsh[:status] }
        @u3 = u3
    end

    def domain_validations
        # ...
        @domainValidations = Service.pendingValidDomain_list # .order(:created_at)
        render json: {
            status: "success",
            domainValidations: @domainValidations
        }
    end

    def validate_domain
        # ... Automated
        puts validation_params
        User.find(validation_params).update(:domain_valid => true)
        render json: {
            status: "success",
        }
        User.find(validation_params).update_deployment_status_domain
    end

    def invalidate_domain
        # Needs to push the user back to invalid
        User.find(validation_params).update(:domain_valid => false)
        render json: {
            status: "success"
        }
    end

    def build_tickets
        @buildTickets = []
        User.most_recent_service_status.each { |f| if f.services.last.status == "deployInProgress" then @buildTickets.push(f) end }

        #@buildTickets = User.joins(:services).where(:services => {status: "deployInProgress"}).limit(20) # .order(:created_at)
        render json: {
            status: "success",
            buildTickets: @buildTickets
        }
    end

    def mark_build_complete
        # puts "Build has been completed."
        # This has to be migrated.. I'm thinking its migrated to a job.
        # Will be called back on.. from the user
        # Moving Scope, see app/controllers/rundeck_controller.rb
        User.find(build_completion_params).update_deployment_status_deployed        
        json_message = User.find(build_completion_params).create_valid_subscription
        render json: json_message
    end


    def decom_tickets
        @decomTickets = []
        User.most_recent_service_status.each { |f| if f.services.last.status == "pendingUnsubscribe" then @decomTickets.push(f) end }
        render json: {
            status: "success",
            decomTickets: @decomTickets
        }
    end

    def mark_decom_complete
        if User.find(decom_completion_params).remove_current_plan[:status] != "success" then render json: {status: "error", message:"Couldn't remove the current plan."} and return  end
        if User.find(decom_completion_params).cancel_account_subscription[:status] != "success" then render json: {status: "error", message:"Couldnt Cancel Subscription"} and return end
        User.find(decom_completion_params).update_deployment_status_decommed
        render json: {status: "success", message:"Account has successfully been removed!"}
    rescue => e
        render json: {status: "error", message: e}
    end

    def support_tickets
    end

    private 

    def validation_params
        params.require(:user_id)
    end

    def build_completion_params
        params.require(:user_id)
    end
    
    def decom_completion_params
        params.require(:user_id)
    end

    # def validation_params
    #     params.permit(:user_id)
    # end
end
