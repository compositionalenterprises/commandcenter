class Rundeck::DatapointsController < ApplicationController
    before_action :authenticate_admin!

    def show
    end

    def environment_job_info
        render json: Rundeck::DatapointsHelper.environment_job_info
    end

    def instance_job_info
        render json: Rundeck::DatapointsHelper.instance_job_info
    end

    def migration_job_info
        render json: Rundeck::DatapointsHelper.migration_job_info
    end
end
