class Rundeck::OptionsController < ApplicationController
    before_action :authenticate_admin!
    
    def show
        @options = RundeckConfigurationOption.all
        render json: { 
            status: "success",
            options: @options
        }
    end

    def update
        if RundeckConfigurationOption.find_by(name: option_params[:name]).update(value: option_params[:value])
            render json: {
                status: "success",
                message: "Successfully updated the Job  ID's!"
            }
        end
    end

    private

    def option_params
        params.require(:option).permit(:name, :value)
    end
end
