class RundeckController < ApplicationController
    # See /app/controller/services_controller.rb#deploy for the "opening" of the loop
    # This controller "closes" the loop

    skip_before_action :verify_authenticity_token
    before_action :authenticate!

    def start
        return_json = nil
        case params[:jobid]
        when RundeckHelper.create_environment_id
            return_json = RundeckHelper.environment_start(user_job)
        when RundeckHelper.create_instance_id
            return_json = RundeckHelper.build_start(user_job)
        when RundeckHelper.migrate_job_id
            return_json = RundeckHelper.migrate_start(user_job)
        when RundeckHelper.delete_job_id
            return_json = RundeckHelper.delete_start(user_job)
        end
        render json: return_json
    end

    def complete
        return_json = nil
        case params[:jobid]
        when RundeckHelper.create_environment_id
            if !user_job.nil?
                RundeckHelper.create_environment_complete(user_job)
            end
            return_json = {"status": "success"}
        when RundeckHelper.create_instance_id
            RundeckHelper.build_complete(user_job)
        when RundeckHelper.migrate_job_id
            return_json = RundeckHelper.migrate_complete(user_job)
        when RundeckHelper.delete_job_id
            return_json = RundeckHelper.delete_complete(user_job)
        end
        render json: return_json
    end
    
    def fail
        return_json = nil
        case params[:jobid]
        when RundeckHelper.create_environment_id
            return_json = RundeckHelper.create_environment_fail(user_job)
        when RundeckHelper.create_instance_id
            return_json = RundeckHelper.create_job_fail(user_job)
        when RundeckHelper.migrate_job_id
            return_json = RundeckHelper.migrate_fail(user_job)
        when RundeckHelper.delete_job_id
            return_json = RundeckHelper.delete_fail(user_job)
        end
        render json: return_json
    end

    private

    def authenticate!
        # needs to get the environment working
        #params[:token] == RundeckConfigurationOption.find_by(name: "AUTH_TOKEN_FROM").value
        params[:token] == RundeckHelper.auth_token_from
    end

    def user_job
        if RundeckJob.find_by(job_id: params[:executionid]).nil?
            return nil
        else
            return RundeckJob.find_by(job_id: params[:executionid])
        end
    end

    def job_params
        # Setup in Rundeck for Callbacks
        # http://ff1980bc.ngrok.io /rundeck/start?jobid=${job.id}&executionid=${execution.id}&token=EXCLUDED_HERE
        # http://ff1980bc.ngrok.io /rundeck/complete?jobid=${job.id}&executionid=${execution.id}&token=EXCLUDED_HERE
        # http://ff1980bc.ngrok.io /rundeck/fail?jobid=${job.id}&executionid=${execution.id}&token=EXCLUDED_HERE
        # Data Returned from Rundeck to Rails
        # Parameters: {"jobid"=>"3f3d2f59-91a9-4724-aada-53dd3aa9d578", "executionid"=>"168", "token"=>"d507aa5b3135c3af80706c69e173ea34b6944be7"}
        # Exec ID matched to the User
        # 
        params.require(:rundeck).permit(:jobid, :executionid)
    end
end
