class Billing::PlansController < ApplicationController
    before_action :authenticate_user!
    def create
        if current_user.customerInformation.blank?
            render json: {
                status: "error",
                message: "You need to add billing information before adding a plan!"
            }
        end
        if current_user.plan == nil
            current_user.create_plan(:stripe_plan_id => convert_plan_parameters(plan_parameters[:plan_name]),  :plan_status => "valid")
            current_user.update_deployment_status_domain
            render json: {
                status: "success",
                message: "You have selected a plan!",
                deploymentStatus: current_user.services.last.status
            }
        else
            if current_user.plan.stripe_plan_id != nil
                current_user.update_deployment_status_domain
                render json: {
                    status: "error",
                    message: "You are already subscribed to this plan: " + current_user.plan.stripe_plan_id,
                    deploymentStatus: current_user.services.last.status
                }
            else
                current_user.plan.destroy
                render json: {
                    status: "error",
                    message: "Looks like you haven't been paying!"
                }
                #current_user.update_deployment_status_pending_decom
            end
        end
    rescue InvalidPlanException => e
        render json: {
            status: "error",
            message: e.message
        }
    end
    def destroy
        if current_user.plan.blank?
            render json: {
                status: "error",
                message: "You have no plans!"
            }
        end
        current_user.update_deployment_status_pending_decom
        render json: {
            status: "success",
            message: "You have successfully been unsubscribed!",
            deploymentStatus: current_user.services.last.status
        }
    rescue => e
        render json: {
            status: "error",
            message: "We ran into an error while unsubscribing you!"
        }
    end

    private
    def plan_parameters
        params.permit(:plan_name)
        # return Plan::PLANS[:minimal]
    end

    def convert_plan_parameters(plan_params)
        case plan_params
        when "minimal"
            return Plan::PLANS[:minimal_plan]
        when "personal"
            return Plan::PLANS[:personal_plan]
        when "personal_plus"
            return Plan::PLANS[:personal_plus_plan]
        else
            raise InvalidPlanException.new("Invalid Plan"), "You have selected an invalid plan!"
        end
    end
end

class InvalidPlanException < StandardError
    attr_reader :name

    def initialize(foo)
     super
     @foo = name
    end
end
