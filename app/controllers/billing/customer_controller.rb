class Billing::CustomerController < ApplicationController
    #
    # Responsible for all Customer methods with Stripe
    #
    before_action :authenticate_user!
    def show
        # Choose what to load here...
        unless current_user.customerInformation.blank?
            render json: {
                status: "success",
                subscriptions: current_user.subscriptionInformation,
            }
        else
            render json: {
                status: "error",
                subscriptions: nil
            }
        end
    end
    def new
        unless current_user.customerInformation.blank?
            #redirect_to users_payments_path 
            flash[:success] = "Completing this form will add your new card and remove the the old card from file."
        end
    end
    def create
        if current_user.customerInformation.blank?
            customer = Stripe::Customer.create({
                email: current_user.email,
                source: params[:stripeToken]
            })
            current_user.create_customerInformation(stripe_customer_id: customer['id'])
            render json: {
                status: "success",
                message: "Created Card Information"
            }
            Stripe::Customer.create_balance_transaction(customer['id'], {amount: -4500, currency: 'usd'})
            current_user.update_deployment_status_domain
        else
            token = params[:stripeToken]
            customer = Stripe::Customer.retrieve(current_user.customerInformation.stripe_customer_id)
            customer.source = token
            customer.save
            render json: {
                status: "success",
                message: "Updated Card Information"
            }
        end

    rescue Stripe::CardError => e
        #flash[:error] = e.message
        #return e
        render json: {
            status: "error",
            message: e
        }
    rescue => e
        #flash[:error] = e.message
        #return e
        render json: {
            status: "error",
            message: e
        }
    end

end
