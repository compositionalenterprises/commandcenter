class Billing::SubscriptionsController < ApplicationController
    #
    # Responsible for subscribing a user to a plan and showing subscriptions
    #
    # Needs revision after due to Plan model!
    #
    #
    # Subscriptions 
    #  - Individual, $20
    #  - Team, $60
    #  - Enterprise, $350? 

    # before_action :authenticate_user!
    # def show
    # end

    # def create
    #     if current_user.customerInformation.blank?
    #         render json: {
    #             status: "error",
    #             message: "You need to add billing information before adding a plan!"
    #         }
    #     end
    #     if !current_user.subscriptionInformation.blank?
    #         if current_user.subscriptionInformation.stripe_subscription_id != nil
    #             render json: {
    #                 status: "error",
    #                 message: "You are already subscribed to this plan" + current_user.subscriptionInformation.stripe_subscription_id
    #             }
    #         end
    #     else
    #         customer_id = current_user.customerInformation.stripe_customer_id
    #         plan_id = current_user.customerInformation.plan_id
    #         subscription = Stripe::Subscription.create({
    #             customer: customer_id,
    #             items: [{plan: plan_id}]
    #         })
    #         current_user.create_subscriptionInformation(:stripe_subscription_id => subscription['id'], :plan_id => "plan_FHmI2YtEAE2AND",  :plan_status => "valid")
    #         current_user.update_deployment_status_account
    #         render json: {
    #             status: "success",
    #             message: "You have successfully been subscribed."
    #         }
    #     end

    # rescue => e
    #     render json: {
    #         status: "error",
    #         message: "We ran into an error while subscribing you to the plan."
    #     }
    #     puts e
    # end
    
    # def destroy
    #     if current_user.subscriptionInformation.blank?
    #         render json: {
    #             status: "error",
    #             message: "You have no subscriptions!"
    #         }
    #     end
    #     subscription_id = current_user.subscriptionInformation.stripe_subscription_id
    #     subscription = Stripe::Subscription.retrieve(subscription_id)
    #     # subscription.delete({at_period_end: true})
    #     # current_user.subscriptionInformations.find_by(plan_id: params[:plan_type]).update(:plan_id => nil, :subscription_id => nil)
    #     subscription.delete
    #     current_user.subscriptionInformation.destroy
    #     #current_user.services.decommission #-> Needs to be implemented
    #     render json: {
    #         status: "success",
    #         message: "You have successfully been unsubscribed!"
    #     }
    # rescue => e
    #     render json: {
    #         status: "error",
    #         message: "We ran into an error while unsubscribing you!"
    #     }
    # end

    private

    def subscription_params
    end
end
