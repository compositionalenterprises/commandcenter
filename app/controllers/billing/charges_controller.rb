class Billing::ChargesController < ApplicationController
    #
    # Responsible for all the stripe methods related to charging a customer and one-offs
    #
    def index
        # return json of all the user data i guess...
        unless current_user.customer_id.blank?
            customer = Stripe::Customer.retrieve(current_user.customer_id)
            default_card_id = customer.default_source
            default_card = customer.sources[:data].find {|x| x[:id] == default_card_id }
            default_card = default_card.last4
            charges = current_user.charges
            respond_to do |format|
                format.json {
                    render :json => {
                        :charges => charges,
                        :last_four => default_card
                    }
                }
            end
        end
    end

    def show
    end
end
