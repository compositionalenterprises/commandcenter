class BillingController < ApplicationController
    def show
        @charges = nil
        if !current_user.customerInformation.nil?
            if !current_user.customerInformation.stripe_customer_id.nil?
                @charges = Stripe::Invoice.list({limit: 24, customer: current_user.customerInformation.stripe_customer_id})
            end
        end
    rescue Stripe::InvalidRequestError => e
        @charges = nil
    end
end
