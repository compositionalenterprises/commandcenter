class StaticpagesController < ApplicationController
    def index
    end

    def contact
        @contact = ContactInformation.new
        @subject = params[:subject]
        @description = params[:description]
    end

    def create_contact
        @contact = ContactInformation.new(contact_params)
        if @contact.valid? && @contact.save
            redirect_to root_url, flash: { success: "Thank you for reaching out! We will reach out to you as soon as we can! Check your email to confirm we recieved this contact!"}
            MatrixNotifyJob.perform_later(contact_params[:subject], "#{contact_params[:email].to_s} -- #{contact_params[:description].to_s}")
            ContactMailer.with(contact: @contact).contact_received.deliver_later
        else
            flash[:alert] = "Please check that all the fields below are not empty and re-submit!"
            render:contact
        end
    end

    private 

    def contact_params
        params.require(:contact_information).permit(:phone_number, :first_name, :last_name, :email, :subject, :description)
    end

end
