class ApplicationController < ActionController::Base
    after_action :track_action

    protected
  
    def track_action
      ahoy.track "Ran action", request.filtered_parameters.to_json
    end
end
