class RundeckJob < ApplicationRecord
    # Store user password in plaintext with user_job
    # when the job starts store password in the row
    # after it comes back use the user_job.password to create the instance
    # 

    belongs_to :user

    STATUS = [
        'started',
        'inprogress',
        'completed',
        'failed'
    ]

end
