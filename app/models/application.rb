class Application < ApplicationRecord
    belongs_to :user
    validate :name, :valid_application

    def self.currently_supported_applications
        # Should probably just populate this list on deploy
        return [
            'akaunting',
            'bitwarden',
            'bookstack',
            'firefly',
            'jekyll',
            'kanboard',
            'nextcloud',
            'rundeck',
            'suitecrm',
            'wordpress'
        ]
    end

    private
    
    def valid_application
        if !(Application.currently_supported_applications.include? self.name)
            errors.add(:name, "name is not valid")
        end
    end


end
