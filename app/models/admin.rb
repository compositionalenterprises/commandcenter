class Admin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable

  def self.chart_data
    # Get Most Recent Service for every user
    return_json = {}
    temp_array = []
    User.all.each do |f|
        temp_array.push(f.services.last)
    end
    temp_array.each do |f|
      Service::STATES.each do |state|
        if return_json[state] == nil
          return_json[state] = 0
        end
        if !f.nil?
          if f.status == state
            return_json[state] += 1
          end
        end
      end
    end
    return return_json
  end
  
  def send_on_create_confirmation_instructions
    return nil
  end
end
