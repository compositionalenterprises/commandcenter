class Plan < ApplicationRecord
    belongs_to :user

    # Using this accounts for plan_id changes across environments
    PLANS = { 
        minimal_plan: Rails.application.credentials.stripe[:minimal_plan], 
        personal_plan: Rails.application.credentials.stripe[:personal_plan], 
        personal_plus_plan: Rails.application.credentials.stripe[:personal_plus_plan], 
        team_plan: Rails.application.credentials.stripe[:team_plan]
    }

    SERVICE_LIMIT = {
        minimal_plan: 2,
        personal_plan: 4,
        personal_plus_plan: 256,
        team_plan: 256,
        empty: 2
    }

    def remap(service_name)
        case  service_name
        when Rails.application.credentials.stripe[:minimal_plan]
            return "minimal_plan"
        when Rails.application.credentials.stripe[:personal_plan]
            return "personal_plan"
        when Rails.application.credentials.stripe[:personal_plus_plan]
            return "personal_plus_plan"
        when Rails.application.credentials.stripe[:team_plan]
            return "team_plan"
        else
            return "empty"
        end
    end

end
