class Product < ApplicationRecord
    #
    # Current Products
    #  InProduction: 
    #    - Portal, Nextcloud, Bitwarden, Wordpress, Kanboard, Rundeck, Jekyll,
    #    - FireflyIII
    #  Coming Soon:
    #    - Email Server, User Management, Bitcoin Node, OAuth Server,
    #    - Discourse, Synapse, PHPmyLDAPAdmin, Gittea, OpenBazaar, 
    #    - NodeBB, Wireguard VPN, OpenVPN, Matomo
    #

    # in_production, coming_soon, name, image_link, short, html
    extend FriendlyId

    friendly_id :name, use: :slugged

    validates :name, presence: true, allow_blank: false

    before_save { self.name.downcase! }

    def self.all_in_production
        self.where(in_production: true)
    end

    def self.coming_soon
        self.where(coming_soon: true)
    end


end
