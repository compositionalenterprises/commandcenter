class SslCertInfo < ApplicationRecord
    belongs_to :dnsRecordName
    belongs_to :user

    # Generate SSL Certificate for the customer and Upload it to RP server
    # parameters
    #   - domain_names (array) (string) - 
    def generate_customer_ssl_certification()
        private_key = OpenSSL::PKey::RSA.new(File.read(File.expand_path('acme_account_client_pk.pem')))
        client = Acme::Client.new(private_key: private_key, directory: 'https://acme-v02.api.letsencrypt.org/directory', kid: 'https://acme-v02.api.letsencrypt.org/acme/acct/50571546')
        domain_name = self.dnsRecordName.full_dns_name
        order = client.new_order(identifiers: domain_name)
        order.authorizations.each do |authorization|
            challenge = authorization.dns
            puts "record name:"
            puts challenge.record_name
            record_name = challenge.record_name
            puts "\nrecord type:"
            puts challenge.record_type
            record_type = challenge.record_type
            puts "\nrecord content:"
            puts challenge.record_content
            record_content = challenge.record_content
            change_id = set_ssl_dns("CREATE", challenge.record_name, challenge.record_type, challenge.record_content, domain_name)
            while get_change_status(change_id).status == "PENDING"
                puts "Pending AWS DNS Changes"
                sleep 5
            end
            challenge.request_validation
            while challenge.status == 'pending'
                sleep(2)
                challenge.reload
            end
            puts challenge.status
            csr = Acme::Client::CertificateRequest.new(private_key: OpenSSL::PKey::RSA.new(File.read(File.expand_path('acme_client_csr_pk.pem'))), subject: { common_name: "#{domain_name}" })
            order.finalize(csr: csr)
            sleep(1) while order.status == 'processing'
            puts "getting certificate for #{domain_name}\n\n\n"
            puts order.certificate
            puts "attempting to write to file"
            f = File.new("./cert-files/#{domain_name[0,domain_name.rindex('.')]}.pem", 'w')
            f.write(order.certificate)
            f.close
            change_id = set_ssl_dns("DELETE", challenge.record_name, challenge.record_type, challenge.record_content, domain_name)
            while get_change_status(change_id).status == "PENDING"
                puts "Pending AWS DNS Changes"
                sleep 5
            end
        end
    end


    def set_ssl_dns(action, record_name, record_type, record_content, domain_name)
        r53 = Aws::Route53::Client.new(region: Rails.application.credentials.aws[:region], access_key_id: Rails.application.credentials.aws[:access_key_id], secret_access_key: Rails.application.credentials.aws[:secret_access_key])
        resp = r53.change_resource_record_sets({
            change_batch: {
                changes: [{
                    action: action, 
                    resource_record_set: {
                        name: "#{record_name}.#{domain_name}", 
                        resource_records: [
                            {value: "\"#{record_content.to_s}\"",},
                        ], 
                        ttl: 60, 
                        type: record_type,
                    },
                }],
                comment: "#{action} Record for SSL", 
            }, 
            hosted_zone_id: Rails.application.credentials.aws[:hosted_zone_id], 
        })
        return resp.to_h[:change_info][:id]
    end

    def get_change_status(change_id)
        r53 = Aws::Route53::Client.new(region: Rails.application.credentials.aws[:region], access_key_id: Rails.application.credentials.aws[:access_key_id], secret_access_key: Rails.application.credentials.aws[:secret_access_key])
        resp = r53.get_change({id: change_id})
        return resp.change_info
    end


end
