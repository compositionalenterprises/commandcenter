class CustomerInformation < ApplicationRecord
    belongs_to :user
    # May need to do some validations
    def has_valid_customer_information()
    end

    def self.all_customers
        stored_customers = []
        starting_after = nil
        customers = Stripe::Customer.list({limit: 10})
        customers.each do |customer|
            stored_customers.append(customer)
        end
        while customers.has_more == true
            customers = Stripe::Customer.list({limit: 10, starting_after: stored_customers.last})
            customers.each do |customer|
                stored_customers.append(customer)
            end
        end
        return stored_customers
    end
    
end
