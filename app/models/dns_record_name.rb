class DnsRecordName < ApplicationRecord
    has_one :sslCertInfo
    belongs_to :user

    # Create a new public DNS A Record for the external Elastic IP to the Reverse Proxy and Return Id of the DNS Change
    # parameters
    #   - public_ip_address - (string) IP address of where to point the dns reord
    #   - customer_name - (string) name of the user or subdomain where the services will be a part of
    #   - services - (array) (string) 
    def create_external_route53_dns_records(customer_name, services)
        r53 = Aws::Route53::Client.new(region: Rails.application.credentials.aws[:region], access_key_id: Rails.application.credentials.aws[:access_key_id], secret_access_key: Rails.application.credentials.aws[:secret_access_key])
        changes = []
        services.each do |service|
            changes << { 
                action: "CREATE", 
                resource_record_set: {
                    name: "#{service}.#{customer_name}.#{Rails.application.credentials.aws[:public_dns_evnironment_domain]}", 
                    resource_records: [
                        {value: Rails.application.credentials.aws[:reverse_proxy_ip],},
                    ], 
                    ttl: 60, 
                    type: "A",
                },
            }
        end
        resp = r53.change_resource_record_sets({
            change_batch: {
                changes: changes, 
                comment: "Set Internal DNS A record for customer: #{customer_name}", 
            }, 
            hosted_zone_id: Rails.application.credentials.aws[:hosted_zone_id], 
        })
        puts "Change id is: #{resp.to_h[:change_info][:id]}"
        return resp.to_h[:change_info][:id]
    end


    # Create a new internal DNS A Record to point the service
    # parameters
    #   - internal_ip_address - (string) IP address of where to point the dns reord
    #   - customer_name - (string) name of the user or subdomain where the services will be a part of
    #   - services - (array, strings) name of the services with a created dns record
    def create_internal_route53_dns_records(internal_ip_address, customer_name, services)
        r53 = Aws::Route53::Client.new(region: Rails.application.credentials.aws[:region], access_key_id: Rails.application.credentials.aws[:access_key_id], secret_access_key: Rails.application.credentials.aws[:secret_access_key])
        changes = []
        services.each do |service|
            changes << { 
                action: "CREATE", 
                resource_record_set: {
                    name: "#{service}.#{customer_name}.#{Rails.application.credentials.aws[:private_dns_evnironment_domain]}", 
                    resource_records: [
                        {value: internal_ip_address,},
                    ], 
                    ttl: 60, 
                    type: "A",
                },
            }
        end
        resp = r53.change_resource_record_sets({
            change_batch: {
                changes: changes, 
                comment: "Set Internal DNS A record for customer: #{customer_name}", 
            }, 
            hosted_zone_id: Rails.application.credentials.aws[:hosted_zone_id], 
        })
        puts "Change id is: #{resp.to_h[:change_info][:id]}"
        return resp.to_h[:change_info][:id]
    end


    # Return Status of DNS Change
    # parameters
    #   - change_id - (string) - The id is returned from creating records, i.e. "/change/C2UBV69SS7" would be an id
    def get_change_status(change_id)
        r53 = Aws::Route53::Client.new(region: Rails.application.credentials.aws[:region], access_key_id: Rails.application.credentials.aws[:access_key_id], secret_access_key: Rails.application.credentials.aws[:secret_access_key])
        resp = r53.get_change({id: change_id})
        return resp.change_info
    end

    
    # Generates an Nginx .conf file to copy over to the reverse proxy server
    # parameters
    #   - customer_name - (string) - the name of the subdomain/customer_name
    #   - services - (array)(string) - a name of the services 
    def generate_reverse_proxy_configuration_file(customer_name, services)
        puts "Generating RP Config File"
        docker_compose_template = ERB.new File.new("./nginx-config-template.erb").read
        services = services
        customer_name = customer_name
        has_certificate = true
        
        public_dns_evnironment_domain = Rails.application.credentials.aws[:public_dns_evnironment_domain]
        private_dns_environment_domain = Rails.application.credentials.aws[:private_dns_evnironment_domain]

        docker_compose_written_file = docker_compose_template.result(binding)

        File.open("./nginx-#{customer_name}.conf", 'w') do |f|
            f.write docker_compose_written_file
        end
        return "nginx-#{customer_name}.conf"
    end

end
