class Service < ApplicationRecord
    belongs_to :user

    # def configured
    #     return Service.where(configured: true)
    # end

    # def online
    #     return Service.where(online: true)
    # end

    def self.pendingValidDomain_list()
      User.joins(:customerInformation).joins(:plan).where.not(domain: nil).where.not(domain: "").where(domain_valid: [false, nil]).all
    end

    # Invalid account means no payment or plan picked out
    # Pending valid domain means waiting on admins to validate domain
    # Pending Deploy means deploy is ready
    # Deploy in Progress means staff is working on build
    # Deploy Complete means services are ready
    # Pending Unsubscribe means the account services are being disabled
    # Unsubscribed means no charges will occur
    STATES = [
      'invalidAccount', 
      'pendingValidDomain', 
      'pendingDeploy', 
      'deployInProgress', 
      'deployComplete', 
      'pendingUnsubscribe', 
      'unsubscribed'
    ]

    # Account Statuses
    ACCOUNT_STATUS = [
      'invalidAccount',
      'validAccount',
      'invalidDomain',
      'validDomain',
      'invalidPayment',
      'validPayment',
      'invalidPlan',
      'validPlan',
      'pendingDeploy',
      'deployInProgress'
    ]

    # 2.0 
    # Invalid account means no payment or plan picked out
    # Pending valid domain means waiting on admins to validate domain
    # Pending Deploy means deploy is ready
    # Deploy in Progress means staff is working on build
    # Deploy Complete means services are ready
      # This is when the subscription to the plan is started
    # Pending Unsubscribe means the account services are being disabled
    # Unsubscribed means no charges will occur

    # Pick a Plan on user side
    # User is subscribed once build is marked complete

end
