class Server < ApplicationRecord

    def self.update_server_instance_data
        client = client()
        client.droplets.all.each do |server|
            if self.find_by(do_id: server.id) != nil
                self.find_by(do_id: server.id).update(do_id: server.id, name: server.name, floating_ip: server.networks.v4[0].ip_address, status: server.status)
            else
                self.create(do_id: server.id, name: server.name, floating_ip: server.networks.v4[0].ip_address, status: server.status, outstanding: false)
            end
        end
    end

    def self.offline_or_no_payment
        current_servers = current_servers_name()
        invalid_servers = []
        current_servers.each do |domain|
            if User.find_by(domain: domain) == nil
                invalid_servers.append(domain)
            end
        end
        return invalid_servers
    end

    def self.matched_servers_to_users
        current_servers = self.all
        matched_users_with_valid_payment = []
        matched_users_invalid_payment = []
        misnamed_droplets = []
        self.all.each do |server|
            if server.name[0,7] == "droplet" || server.name[0,6]== "ubuntu"
                misnamed_droplets.append(server.name)
                next
            end
            index = server.name.index('--')
            user = User.find_by(domain: server.name[0, index].gsub('-', '.'))
            if user != nil
                if !user.subscriptionInformation.nil?
                    if user.subscriptionInformation.plan_status == "valid"
                        matched_users_with_valid_payment.append(user)
                    else
                        matched_users_invalid_payment.append(user)
                    end
                else
                    matched_users_invalid_payment.append(user)
                end
            end
        end
        return {"matched_users_with_valid_payment": matched_users_with_valid_payment, "matched_users_invalid_payment": matched_users_invalid_payment, "misnamed_droplets": misnamed_droplets}
    end

    def self.match_server_to_user
        updated_servers = []
        names = self.all.each do |f|
            index = f.name.index('--')
            user = User.find_by(domain: f.name[0, index].gsub('-', '.'))
            if user != nil
                f.update(user_id: user.id)
                updated_servers.append(user)
            end
        end
        return updated_servers
    end
    
    def self.current_servers_name
        client = client()
        server_names = []
        client.droplets.all.each do |f|
            index = f.name.index('--')
            server_names.append(f.name[0, index].gsub('-', '.'))
        end
        return server_names
    end

    def self.current_servers_name_offline
        server_names = []
        misnamed_droplets = []
        self.all.each do |f|
            if f.name[0,7] == "droplet" || f.name[0,6]== "ubuntu"
                misnamed_droplets.append(f.name)
                next
            end
            index = f.name.index('--')
            server_names.append(f.name[0, index].gsub('-', '.'))
        end
        return server_names
    end

    def self.client
        client = nil
        if ENV['DO_API_KEY'] != nil
            client = DropletKit::Client.new(access_token: ENV['DO_API_KEY'])
        else
            client = DropletKit::Client.new(access_token: Rails.application.credentials.do[:api_key])
        end
        return client
    end
end
