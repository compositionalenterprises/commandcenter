class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  validates :domain, :uniqueness => {:case_sensitive => false}, :allow_nil => true, :allow_blank => true, length: {minimum: 2, maximum: 32}
  validate :domain, :domain_not_locked

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable, :trackable

  has_one :customerInformation
  has_one :subscriptionInformation
  has_one :plan
  has_one :keyPair
  has_one :ec2Instance
  has_one :ec2InstanceProvisionStatus
  has_one :dnsRecordName
  has_many :sslCertInfo
  has_many :services
  has_many :rundeck_jobs
  has_one :server
  has_many :applications

  after_create :set_services_invalid, :generate_vault_password

  scope :most_recent_service_status, -> { joins(:services)
    where('services.created_on = (SELECT MAX(services.created_on) FROM services WHERE services.user_id = users.id)')
    group('users.id')
  }

  def encrypted_vault_password
    salt = Rails.application.credentials.encryption[:salt]
    key   = ActiveSupport::KeyGenerator.new(Rails.application.credentials.encryption[:key]).generate_key(salt, 32)
    crypt = ActiveSupport::MessageEncryptor.new(key)
    if self.read_attribute(:encrypted_vault_password) != nil
      return crypt.decrypt_and_verify(self.read_attribute(:encrypted_vault_password))
    else
      return nil
    end
  end

  def encrypted_vault_password=(val)
    salt = Rails.application.credentials.encryption[:salt]
    key   = ActiveSupport::KeyGenerator.new(Rails.application.credentials.encryption[:key]).generate_key(salt, 32)
    crypt = ActiveSupport::MessageEncryptor.new(key)
    encrypted_vp = crypt.encrypt_and_sign(val)
    self.write_attribute(:encrypted_vault_password, encrypted_vp)
  end

  def self.all_domains
    domains = []
    self.all.each do |f|
      domains.append({user_id: f.id,  domain: f.domain})
    end
    return domains
  end

  def has_valid_domain? 
    if (self.domain == nil || self.domain == "") || self.domain_valid != true
      return false
    else
      return true
    end
  end

  def service_limit
    Plan::SERVICE_LIMIT[self.plan.remap(self.plan.stripe_plan_id).to_sym]
  end

  def current_applications_s
    # Returns string format to be sent to Rundeck API
    stringval = ""
    self.applications[0..self.service_limit].each do |f|
      stringval = stringval + f.name + ","
    end
    return stringval
  end

  def has_valid_applications_selected?
    if !(self.applications.count == 0)
      return true
    else
      return false
    end
  end

  def deploy_in_progress?
    if !self.services.find_by(status: "deployInProgress").nil?
      return true
    else
      return false
    end
  end

  def has_valid_account?
    if !self.customerInformation.blank? && !self.plan.blank? && self.has_valid_domain?
      return true
    else
      return false
    end
  end

  def needs_domain_validated?
    if !self.customerInformation.blank? && !self.plan.blank? && !self.has_valid_domain?
      return true
    else
      return false
    end
  end

  def has_services_deployed?
    if !self.services.blank? || !self.services.count == 0
      if !self.services.find_by(status: "deployComplete").nil?
        return true
      else
        return false
      end
    else
      return false
    end
  end

  def deployment_status
    if !self.services.blank?
      self.services.order("created_at").last.status
    else
      return nil
    end
  end

  def set_services_invalid
    self.services.create(:status => "invalidAccount")
  end

  def generate_vault_password
    random = SecureRandom.hex(14)
    self.encrypted_vault_password = random
    self.save!
  end

  def update_deployment_status_account
    if !self.customerInformation.blank? && !self.plan.blank? && self.services.where(:status => "pendingValidDomain").nil?
      self.services.create(:status => "pendingValidDomain")
    else
      self.services.create(:status => "pendingDeploy")
    end
  end

  def update_deployment_status_deployed
    self.update_attribute(:domain_locked, true)
    self.services.create(:status => "deployComplete")
  end

  def update_deployment_status_pending_decom
    self.services.create(:status => "pendingUnsubscribe")
  end

  def update_deployment_status_decommed
    self.update_attribute(:domain_locked, false)
    self.services.create(:status => "unsubscribed")
    self.subscriptionInformation.update(plan_status: "unsubscribed")
    SignupAlertJob.perform_later(self.domain, "requested decommision")
  end

  def update_deployment_status_domain
    self.update_attribute(:domain_valid, true)
    self.services.create(:status => "pendingDeploy")
  end

  def create_valid_subscription
    if self.customerInformation.blank?
      return {status:"error", message:"You need to add billing information before adding a plan!"}
    end
    if !self.subscriptionInformation.blank?
      if self.subscriptionInformation.stripe_subscription_id != nil
        self.update_deployment_status_deployed
        return {status: "success", message: "User was already subscribed!"}
      else
        return {status: "error", message: "User does not have a valid subscription"}
      end
    else
      customer_id = self.customerInformation.stripe_customer_id
      if self.plan.blank?
        return {status:"error", message:"You don't have a plan."}
      end
      if !self.plan.blank?
        if self.plan.stripe_plan_id == nil
          return {status:"error", message: "You don't have a valid plan."}
        end
      end
      plan_id = self.plan.stripe_plan_id
      subscription = Stripe::Subscription.create({
        customer: customer_id,
        items: [{price: self.plan.stripe_plan_id}]
      })
      self.create_subscriptionInformation(:stripe_subscription_id => subscription['id'], :plan_id => self.plan.stripe_plan_id,  :plan_status => "valid")
      self.update_deployment_status_deployed
      return {status: "success", message: "You have successfully been subscribed."}
    end
  rescue => e
    return {status: "error", message: e}
  end

  def cancel_account_subscription
    if self.subscriptionInformation.blank?
      return {status: "success", message: "User does not have subscriptions!"}
    end
    subscription_id = self.subscriptionInformation.stripe_subscription_id
    subscription = Stripe::Subscription.retrieve(subscription_id)
    # subscription.delete({at_period_end: true})
    # current_user.subscriptionInformations.find_by(plan_id: params[:plan_type]).update(:plan_id => nil, :subscription_id => nil)
    subscription.delete
    self.subscriptionInformation.destroy
    #current_user.services.decommission #-> Needs to be implemented for automation piece
    RundeckHelper.delete_instance(self.domain, self.encrypted_vault_password)
    User.applications.destroy_all
    return { status: "success", message: "User has successfully been unsubscribed!"}
  rescue => e
    return {status: "error", message: "We ran into an error while unsubscribing User!"}
  end

  def remove_current_plan
    if !self.plan.nil?
      self.plan.destroy
      return {status: "success", message: "Removed current plan from user"}
    else
      return {status: "success", message: "Plan has been removed! Check this user!"}
    end
  rescue => e
    print(e)
    return {status: "error", message: "We ran into an error removing the plans from this user!"}
  end
  
  private

  def domain_not_locked
    if self.domain_changed?
      errors.add(:domain, "can't change a locked domain") if self.domain_locked == true
    end
  end

end
