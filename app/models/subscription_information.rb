class SubscriptionInformation < ApplicationRecord
    belongs_to :user
    def has_valid_plan
    end
    
    def self.valid_plans
        return self.all.where(plan_status:'valid')
    end
    # Subscription Information
    # Pull down Customer Info Periodically

    def self.current_subscriptions
        stored_subs = []
        starting_after = nil
        subscriptions = Stripe::Subscription.list({limit: 10})
        subscriptions.each do |subscription|
            stored_subs.append(subscription)
        end
        while subscriptions.has_more == true
            subscriptions = Stripe::Subscription.list({limit: 10, starting_after: stored_subs.last})
            subscriptions.each do |subscription|
                stored_subs.append(subscription)
            end
        end
        return stored_subs
    end



end
