class KeyPair < ApplicationRecord
    belongs_to :user

    # Create new key-pair for customer and returns (array) (string) -  key_pair_name, key_fingerprint, key_decrypted_rsa_private_key
    # parameters
    #   - key_pair_name (string) - name of the key pair, should be unique and should itentify the customer
    def create_new_key_pair()
        client = Aws::EC2::Client.new(region: Rails.application.credentials.aws[:region], access_key_id: Rails.application.credentials.aws[:access_key_id], secret_access_key: Rails.application.credentials.aws[:secret_access_key])
        begin 
            key_pair = ec2.create_key_pair({
                key_name: self.user.wanted_domain
            })
            puts [key_pair.key_name, key_pair.key_fingerprint, key_pair.key_material].to_s
            return [key_pair.key_name, key_pair.key_fingerprint, key_pair.key_material]
        rescue Aws::EC2::Errors::InvalidKeyPairDuplicate
            puts "Key Pair #{self.user.wanted_domain} already exists."
            return nil
        end
    end


end
