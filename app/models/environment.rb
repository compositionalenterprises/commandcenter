class Environment < ApplicationRecord
    @@PROJECT_ID = 16073723

    # Searches gitlab for the environmnets repo
    # Returns an array of the name of the project and the repo id
    # Looking for the project named `environment`
    def self.search_gitlab_projects
        json_data = []
        Gitlab.projects(visibility: "private", search: "environment").each do |f|
            json_data.append({name: f.name, id: f.id})
        end
        return json_data
    end

    # Job to fill the environment branches
    # Update the database with the current environment from Gitlab
    # Returns an array of environments that were added
    def self.update_ourcompose_gitlab_environments
        updated = []
        Gitlab.branches(@@PROJECT_ID).each do |branch|
            if self.find_by(gitlab_name: branch.name).nil?
                self.create(gitlab_name: branch.name, gitlab_commit_created_at: branch.commit.created_at, gitlab_commit_title: branch.commit.title, gitlab_commit_authored_date: branch.commit.authored_date)
                updated.append({name: branch.name, gitlab_commit_created_at: branch.commit.created_at, gitlab_commit_authored_date: branch.commit.authored_date})
            end
        end
        return updated
    end

    # Pulls all the branches from Gitlab
    def self.get_ourcompose_gitlab_environments
        Gitlab.branches(@@PROJECT_ID)
    end

    # Rather than pulling from Gitlab, this method returns the environments in our database
    def self.get_ourcompose_gitlab_environmnets
        self.all
    end

    # Returns an array of the environments currently in our database
    def self.get_environments
        environments = []
        self.all.each do |environment|
            environment_name = environment.gitlab_name.gsub('-', '.')
            environments.append(environment_name)
        end
        return environments
    end

    def self.subscription_without_environment
        no_environment = []
        SubscriptionInformation.where(plan_status: "valid").each do |subinfo|
            user = User.find(subinfo.user_id)
            environment = self.get_environments.index(user.domain)
            if environment.nil?
                no_environment.append(user.domain)
            end
        end
        return no_environment
    end

    def self.subscription_with_env_no_instance
        env_no_instance = []
        self.get_environments.each do |f|
            server = Server.current_servers_name_offline.index(f)
            if server.nil?
                env_no_instance.append(f)
            end
        end
        return env_no_instance
    end

end
