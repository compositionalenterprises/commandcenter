class ConciergeMailer < ApplicationMailer
    default from: Rails.application.credentials.email[:host]

    # ConciergeMailer.with(user: User.first, password: `password`).new_signup.deliver_later
    def new_signup
        @password = params[:password]
        @user = params[:user]
        mail(to: @user.email, subject: 'OurCompose Concierge Signup')
    end

end
