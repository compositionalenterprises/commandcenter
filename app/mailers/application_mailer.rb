class ApplicationMailer < ActionMailer::Base
  default from: Rails.application.credentials.email[:host]
  layout 'mailer'
end
