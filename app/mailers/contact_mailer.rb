class ContactMailer < ApplicationMailer
    default from: Rails.application.credentials.email[:host]
 
    def contact_received 
      @contact = params[:contact]
      mail(to: @contact.email, subject: 'Contact Submission')
    end

    def instance_complete
      @user = params[:user]
      mail(to: @user.email, subject: 'Instance Ready')
    end
    
end
