class StartDeployJob < ApplicationJob
  queue_as :default

  def perform(domain, vault_password, services, admin_password)
    RundeckHelper.start_deploy(domain, vault_password, services, admin_password)
  rescue RundeckHelper::RundeckHelperError
    puts "Error in the RundeckHelper, fool."
    SignupAlertJob.perform_later(domain, "Error Deploying. Manual inspection needed.")
  end
end
