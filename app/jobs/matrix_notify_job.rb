class MatrixNotifyJob < ApplicationJob
  queue_as :default

  def perform(subject, message)
    subject = subject
    message = message
    if subject.blank?
      subject = "Empty"
    end
    if message.blank?
      message = "has no message."
    end
    api = MatrixSdk::Api.new 'https://matrix.org'
    username = ourcompose_username
    password = ourcompose_password
    a = api.login user: username, password: password, no_sync: true, cache: :none
    access_token = a[:access_token]
    client = MatrixSdk::Client.new 'https://matrix.org'
    client.api.access_token = access_token
    room_name = ourcompose_alerts_room
    room = client.join_room(room_name)
    room.send_text "#{subject} -- #{message}"
  rescue MatrixSdk::MatrixRequestError
    puts "Matrix Error, sheet.. check your creds, son."
    puts "Error performing MatrixNotifyJob"
    return nil
  end

  private
  # Stolen from ./signup_alert_job

  def ourcompose_username
    username = nil
    if Rails.application.credentials.matrix[:username] != nil
      if Rails.application.credentials.matrix[:username] != 'test'
        username = Rails.application.credentials.matrix[:username]
      else
        username = ENV['CHAT_OURCOMPOSEBOT_USERNAME']
      end
    else
      username = ENV['CHAT_OURCOMPOSEBOT_USERNAME']
    end
    return username
  end

  def ourcompose_password
    password = nil
    if Rails.application.credentials.matrix[:password] != nil
      if Rails.application.credentials.matrix[:password] != 'test'
        password = Rails.application.credentials.matrix[:password]
      else
        password = ENV['CHAT_OURCOMPOSEBOT_PASSWORD']
      end
    else
      password = ENV['CHAT_OURCOMPOSEBOT_PASSWORD']
    end
    return password
  end

  def ourcompose_alerts_room
    room = nil
    if Rails.application.credentials.matrix[:room] != nil
      if Rails.application.credentials.matrix[:room] != 'test'
        room = Rails.application.credentials.matrix[:room]
      else
        room = ENV['CHAT_OURCOMPOSEBOT_ROOM']
      end
    else
      room = ENV['CHAT_OURCOMPOSEBOT_ROOM']
    end
    return room
  end
end
