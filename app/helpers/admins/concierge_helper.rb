module Admins::ConciergeHelper

    def self.create_user(email, domain, password, applications) 
        us = User.new(email: email, domain: domain, password: password, password_confirmation: password)
        us.save!
        us.validate
        applications.each do |app|
            a = us.applications.new(name: app)
            a.save!
        end
        return us
 
    rescue => e
        return nil
    end

    def self.create_stripe_customer(user_object, stripe_token)
        if user_object.customerInformation.blank?
            customer = Stripe::Customer.create({
                email: user_object.email,
                source: stripe_token
            })
            user_object.create_customerInformation(stripe_customer_id: customer['id'])
            render json: {
                status: "success",
                message: "Created Card Information"
            }
            Stripe::Customer.create_balance_transaction(customer['id'], {amount: -4500, currency: 'usd'})
            user_object.update_deployment_status_domain
        else
            raise ConciergeHelperError, "Customer Already Exists"
        end
    rescue Stripe::CardError => e
        return e
    rescue => e
        return e
    end
end


class ConciergeHelperError < StandardError
end