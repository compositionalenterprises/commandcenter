module ProductsHelper
    def self.picture_exists?(product)
        zendings = ["_logo.png", "_logo.jpg", "_logo.svg", "_logo.svg.png"]
        zendings.each do |ending|
            begin
                if ActionController::Base.helpers.asset_pack_path("media/images/products/" + product + ending).is_a?(String)
                    return true
                end
            rescue Webpacker::Manifest::MissingEntryError
                next
            end
        end
        return false
    end

    def self.picture_path(product)
        zendings = ["_logo.png", "_logo.jpg", "_logo.svg", "_logo.svg.png"]
        zendings.each do |ending|
            begin
                if ActionController::Base.helpers.asset_pack_path("media/images/products/" + product + ending).is_a?(String)
                    returnstr = "media/images/products/" + product + ending
                    return returnstr
                end
            rescue Webpacker::Manifest::MissingEntryError
                next
            end
        end
        return ""
    end
end
