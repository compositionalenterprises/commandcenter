module Rundeck::DatapointsHelper
    include RundeckHelper
    # Rundeck Data

    def self.environment_job_info
        jobs_url = "/rundeck/api/33/job/" + RundeckHelper.create_environment_id + "/executions"
        json_response = JSON.parse(RundeckHelper.rundeck_get(jobs_url))
        statuses = statuses(json_response['executions'])
        return statuses
    end

    def self.instance_job_info
        jobs_url = "/rundeck/api/33/job/" + RundeckHelper.create_instance_id + "/executions"
        json_response = JSON.parse(RundeckHelper.rundeck_get(jobs_url))
        statuses = statuses(json_response['executions'])
        return statuses
    end

    def self.migration_job_info
        jobs_url = "/rundeck/api/33/job/" + RundeckHelper.migrate_job_id + "/executions"
        json_response = JSON.parse(RundeckHelper.rundeck_get(jobs_url))
        statuses = statuses(json_response['executions'])
        return statuses
    end

    def self.statuses(jobs)
        succeeded = []
        failed = []
        aborted = []
        running = []
        timedout = []

        jobs.each do |job|
            case job['status']
            when "succeeded"
                succeeded.append(job)
            when "failed"
                failed.append(job)
            when "aborted"
                aborted.append(job)
            when "running"
                running.append(job)
            when "timedout"
                timedout.append(job)
            end
        end

        statuses = { succeeded: succeeded, failed: failed, aborted: aborted, running: running, timedout: timedout }
        return statuses
    end

end
