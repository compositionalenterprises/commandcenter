require 'net/http'

module RundeckHelper

    def self.attr_value(*args)
        # Need to fix.. production is caching empty database hits, FUCK
        singleton_class = class << self; self; end
        args.each do |key|
            name = key
            val = nil
            key2 = key.downcase
            if !Rails.application.credentials.rundeck[name].blank?
                val = Rails.application.credentials.rundeck[name]
            else
                val = nil
            end
            singleton_class.send(:define_method, name) do
                return val
            end
        end
    end

    attr_value :base_url, :project_id, :create_environment_id, :create_instance_id, :rundeck_url, :auth_token_to, :migrate_job_id, :delete_job_id, :auth_token_from

    def self.get_jobs
        # Test method
        jobs_url = "/rundeck/api/14/project/" + self.project_id + "/jobs"
        json_response = JSON.parse(self.rundeck_get(jobs_url))
    end

    def self.create_environment(domain, vault_password, services)
        # Needs to be tested
        project_url = "/rundeck/api/14/job/#{self.create_environment_id}/run"
        query_data = "-domain #{domain} -vaultpass #{vault_password} -Services #{services} -email #{User.find_by(domain: domain).email} -dropletsize #{self.convert_plan(User.find_by(domain: domain).plan.stripe_plan_id)}"

        response = self.rundeck_post(project_url, query_data)
        puts response

        json_response = JSON.parse(response)
        puts json_response
        return json_response
    rescue JSON::ParserError
        raise RundeckHelperError.new("parse error, fool. you can thank JSON for that one", "create_environment")
    end

    def self.create_instance(user_job)
        # Needs to be tested
        if user_job.nil?
            return { user: "nil" }
        end
        create_instance = self.create_instance_helper(user_job.user.domain, user_job.user.encrypted_vault_password, user_job.temp_pw)
        RundeckJob.create(job_id: create_instance["id"], user_id: user_job.user.id, description: create_instance["status"])
    end

    def self.create_instance_helper(domain, vault_password, admin_pw)
        # Needs to be tested
        project_url = "/rundeck/api/14/job/#{self.create_instance_id}/run"
        form_data = "-domain #{domain} -envvaultpass #{vault_password} -envadminpass #{admin_pw}"
        response = self.rundeck_post(project_url, form_data)

        json_response = JSON.parse(response)
        puts json_response
        return json_response
    rescue JSON::ParserError
        raise RundeckHelperError.new("parse error, fool. you can thank JSON for that one", "create_instance_helper")
    end

    def self.migrate_instance
        return "migrating instance"
    end

    def self.delete_instance(domain, vault_password)
        # Needs to be tested
        project_url = "/rundeck/api/14/job/#{self.delete_job_id}/run"
        form_data = "-domain #{domain} -envvaultpass #{vault_password}"
        response = self.rundeck_post(project_url, form_data)

        json_response = JSON.parse(response)
        puts json_response
        return json_response
    rescue JSON::ParserError
        raise RundeckHelperError.new("parse error, fool. you can thank JSON for that one", "create_instance_helper")
    end


    ## Start Methods

    def self.environment_start(user_job)
        if user_job.nil?
            return { user: "nil" }
        end
        user_job.update(description: "inprogress")
    end

    def self.start_deploy(domain, vault_password, services, admin_password)
        create_instance = self.create_environment(domain, vault_password, services)
        RundeckJob.create(job_id: create_instance["id"], user_id: User.find_by(domain: domain).id, description: create_instance["status"], temp_pw: admin_password)
    end

    def self.build_start(user_job)
        if user_job.nil?
            return { user: "nil" }
        end
        user_job.update(description: "inprogress")
    end

    def self.delete_start(user_job)
        if user_job.nil?
            return { user: "nil" }
        end
        user_job.update(description: "inprogress")
    end

    def self.migrate_start(user_job)
        if user_job.nil?
            return { user: "nil" }
        end
        user_job.update(description: "inprogress")
    end


    ## Complete Methods

    def self.create_environment_complete(user_job)
        if user_job.nil?
            return { user: "nil" }
        end
        user_job.update(description: "complete")
        self.create_instance(user_job)
    end

    def self.build_complete(user_job)
        if user_job.nil?
            return { user: "nil" }
        end
        user_job.update(description: "completed")
        User.find(user_job.user_id).update_deployment_status_deployed
        ContactMailer.with(user: User.find(user_job.user_id)).instance_complete.deliver_later    
        json_message = User.find(user_job.user_id).create_valid_subscription
        return json_message
    end

    def self.delete_complete(user_job)
        if User.find(user_job.user_id).remove_current_plan[:status] != "success" then render json: {status: "error", message:"Couldn't remove the current plan."} and return  end
        if User.find(user_job.user_id).cancel_account_subscription[:status] != "success" then render json: {status: "error", message:"Couldnt Cancel Subscription"} and return end
        User.find(user_job.user_id).update_deployment_status_decommed
        return {status: "success", message:"Account has successfully been removed!"}
    rescue => e
        return {status: "error", message: e}
    end

    def self.migrate_complete(user_job)
    end


    ## Fail Methods

    def self.create_environment_fail(user_job)
        user_job.update(description: "failed")
        SignupAlertJob.perform_later(user_job.user.domain, "rundeck failed at create environment")
    end

    def self.create_job_fail(user_job)
        user_job.update(description: "failed")
        SignupAlertJob.perform_later(user_job.user.domain, "rundeck failed at creating the instance")
        # create_instance = self.create_instance(user_job)
    end

    def self.delete_fail(user_job)
        user_job.update(description: "failed")
        SignupAlertJob.perform_later(user_job.user.domain, "rundeck failed at deleting the instance")
    end

    def self.migrate_fail(user_job)
        user_job.update(description: "failed")
        SignupAlertJob.perform_later(user_job.user.domain, "rundeck failed at creating the instance")
    end

    def self.rundeck_get(query_string)
        uri = URI(self.rundeck_url + query_string)

        req = Net::HTTP::Get.new(uri)
        req['X-Rundeck-Auth-Token'] = self.auth_token_to
        req['Accept'] = 'application/json'
        req['Content-Type'] = 'application/json'

        res = Net::HTTP.start(uri.hostname, uri.port, :use_ssl => uri.scheme == 'https') {|http| http.request(req)}

        return res.body
    end

    def self.rundeck_post(query_string, form_data)
        uri = URI(self.rundeck_url.to_s + query_string)

        req = Net::HTTP::Post.new(uri)
        req['X-Rundeck-Auth-Token'] = self.auth_token_to
        req['Accept'] = 'application/json'
        req.set_form_data("argString" => form_data)

        res = Net::HTTP.start(uri.hostname, uri.port, :use_ssl => uri.scheme == 'https') {|http| http.request(req)}

        return res.body
    end

    def self.convert_plan(plan)
        droplet_size = "minimal"
        case plan
        when Rails.application.credentials.stripe[:minimal_plan]
            droplet_size = "s-1vcpu-1gb"
        when Rails.application.credentials.stripe[:personal_plan]
            droplet_size = "s-1vcpu-1gb"
        when Rails.application.credentials.stripe[:personal_plus_plan]
            droplet_size = "s-1vcpu-2gb"
        end
    end

    class RundeckHelperError < StandardError
        attr_reader :method
        def initialize(msg="RundeckHelper Error", method="")
            @method = method
            super(msg)
        end
    end

end
