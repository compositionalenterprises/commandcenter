import React, { Component } from 'react';
import RundeckConfigurationOptions from './admin/RundeckConfigurationOptions';
import ReactDOM from 'react-dom';
import "core-js";
import 'chart.js';


export default class AdminDashboard extends Component {
    constructor(props){
        super(props);
        this.state = {
            domainValidationsCount: this.props.domainValidationsCount,
            domainValidations: [],
            domainsView: false,
            buildTicketsCount: this.props.buildTicketsCount,
            buildTickets: [],
            buildsView: false,
            decomTicketsCount: this.props.decomTicketsCount,
            decomTickets: [],
            decomsView: false,
            configView: false,
            options: [],
            supportTicketsCount: this.props.supportTicketsCount,
            deployedUsers: this.props.deployedUsers,
        }
        this.getDomainValidations = this.getDomainValidations.bind(this)
        this.getBuildTickets = this.getBuildTickets.bind(this)
        this.getDecomTickets = this.getDecomTickets.bind(this)
        this.removeUserSuccessfulResponse = this.removeUserSuccessfulResponse.bind(this)
        this.removeCompletedBuild = this.removeCompletedBuild.bind(this)
        this.removeCompletedDecom = this.removeCompletedDecom.bind(this)
        this.domainValidationsMinusEqualOne = this.domainValidationsMinusEqualOne.bind(this)
        this.buildCompleteMinusEqualOne = this.buildCompleteMinusEqualOne.bind(this)
        this.decomCompleteMinusEqualOne = this.decomCompleteMinusEqualOne.bind(this)
        this.getConfigurationOptions = this.getConfigurationOptions.bind(this)
    }
    componentDidMount(){
        fetch("admins/chart_data")
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              chartDataLoaded: true,
              chartItems: result.items
            });
            var ctx = document.getElementById('adminOverviewChart').getContext('2d');
            var chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'horizontalBar',

                // The data for our dataset
                data: {
                    labels: Object.keys(result),
                    datasets: [{
                        backgroundColor: ['#007bff','#dc3545','#007bff','#dc3545','#007bff','#dc3545', '#007bff'],
                        label: 'User Count',
                        borderColor: 'rgb(248,249,250)',
                        data: Object.values(result)
                    }]
                },

                // Configuration options go here
                options: {
                    responsive: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                max: Math.round(Math.max(...Object.values(result))*1.125),
                                stepValue: Math.round(Math.max(...Object.values(result))*1.125)/8
                            }          
                        }], 
                        xAxes: [{
                            ticks: {
                                max: Math.round(Math.max(...Object.values(result))*1.125),
                                stepValue: Math.round(Math.max(...Object.values(result))*1.125)/8
                            }
                        }]
                    }
                }
            });
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            this.setState({
              chartDataLoaded: true,
              error
            });
          }
        )
    }
    async getConfigurationOptions(e){
        this.setState({buildsView: false, domainsView:false, decomsView: false, configView: true})
        let response = await fetch("rundeck/options", {
            method: "GET",
            headers: {"Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: null
        });
        if (response.ok){
            let responseJSON = await response.json()
            if (responseJSON.status == "success") {
                this.setState({options: responseJSON.options})
            } else {
                console.log("Error")
                // Update the state to reflect the error
                // Render an error based on the message
            }
        }
    }
    async getDomainValidations(e){
        this.setState({buildsView: false, domainsView:true, decomsView: false, configView: false})
        let response = await fetch("admins/validations", {
            method: "GET",
            headers: {"Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: null
        });
        if (response.ok){
            let responseJSON = await response.json()
            if (responseJSON.status == "success") {
                this.setState({domainValidations: responseJSON.domainValidations})
                //console.log(this.state.domainValidations)
            } else {
                console.log("Error")
                // Update the state to reflect the error
                // Render an error based on the message
            }
        }
    }
    async getBuildTickets(e){
        this.setState({buildsView: true, domainsView:false, decomsView: false, configView: false})
        let response = await fetch("admins/builds", {
            method: "GET",
            headers: {"Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: null
        });
        if (response.ok){
            let responseJSON = await response.json()
            if (responseJSON.status == "success") {
                this.setState({buildTickets: responseJSON.buildTickets})
            } else {
                console.log("Error")
                // Update the state to reflect the error
                // Render an error based on the message
            }
        }
    }
    async getDecomTickets(e){
        this.setState({buildsView: false, domainsView:false, decomsView:true, configView: false})
        let response = await fetch("admins/decoms", {
            method: "GET",
            headers: {"Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: null
        });
        if (response.ok){
            let responseJSON = await response.json()
            if (responseJSON.status == "success") {
                this.setState({decomTickets: responseJSON.decomTickets})
            } else {
                console.log("Error")
                // Update the state to reflect the error
                // Render an error based on the message
            }
        }
    }
    removeCompletedBuild(user_id){
        const indexOfE = this.state.buildTickets.findIndex(x => x.id === user_id)
        const updatedBuildTickets = this.state.buildTickets
        for(var i = 0 ; i < updatedBuildTickets.length; i++){
            if(i == indexOfE){
                updatedBuildTickets.splice(i, 1)
            }
        }
        // console.log(updatedBuildTickets)
        this.setState({buildTickets: updatedBuildTickets})
    }
    removeCompletedDecom(user_id){
        const indexOfE = this.state.decomTickets.findIndex(x => x.id === user_id)
        const updatedDecomTickets = this.state.decomTickets
        for(var i = 0 ; i < updatedDecomTickets.length; i++){
            if(i == indexOfE){
                updatedDecomTickets.splice(i, 1)
            }
        }
        // console.log(updatedBuildTickets)
        this.setState({decomTickets: updatedDecomTickets})
    }
    removeUserSuccessfulResponse(user_id){
        const indexOfE = this.state.domainValidations.findIndex(x => x.id === user_id)
        const updatedDomainValidations = this.state.domainValidations
        for(var i = 0 ; i < updatedDomainValidations.length; i++){
            if(i == indexOfE){
                updatedDomainValidations.splice(i, 1)
            }
        }
        // console.log(updatedDomainValidations)
        this.setState({domainValidations: updatedDomainValidations})
    }
    //Needs to be tested.. may not need this method
    domainValidationsMinusEqualOne(){
        var datCount = this.state.domainValidationsCount
        datCount = datCount - 1
        this.setState({
            domainValidationsCount: datCount
        })
    }
    buildCompleteMinusEqualOne(){
        var datCount = this.state.buildTicketsCount
        datCount = datCount - 1
        this.setState({
            buildTicketsCount: datCount
        })
    }
    decomCompleteMinusEqualOne(){
        var decomCount = this.state.decomTicketsCount
        decomCount = decomCount - 1
        this.setState({
            decomTicketsCount: decomCount
        })
    }
    render() {
        let primaryView;
        if(this.state.domainsView == true){
            primaryView = <DomainValidations 
                domainValidations={this.state.domainValidations}
                removeUserSuccessfulResponse={this.removeUserSuccessfulResponse}
                domainValidationsMinusEqualOne={this.domainValidationsMinusEqualOne}
            />
        }
        if(this.state.buildsView == true){
            primaryView = <BuildTickets 
                buildTickets={this.state.buildTickets}
                removeCompletedBuild={this.removeCompletedBuild}
                buildCompleteMinusEqualOne={this.buildCompleteMinusEqualOne}
            />
        }
        if(this.state.decomsView == true){
            primaryView = <DecomTickets
                decomTickets={this.state.decomTickets}
                removeCompletedDecom={this.removeCompletedDecom}
                decomCompleteMinusEqualOne={this.decomCompleteMinusEqualOne}
            />
        }
        if(this.state.configView == true){
            primaryView = <RundeckConfigurationOptions options={this.state.options}/>
        }
        return (
            <React.Fragment>
                <div className="row col-sm-12 d-flex justify-content-center">
                    <button onClick={this.getDomainValidations} className="btn btn-primary mr-3">Domain Validations <span className="badge badge-light">{this.state.domainValidationsCount}</span></button>
                    <button onClick={this.getBuildTickets} className="btn btn-primary mr-3">Build Tickets <span className="badge badge-light">{this.state.buildTicketsCount}</span></button>
                    <button onClick={this.getDecomTickets} className="btn btn-primary mr-3">Decom Tickets <span className="badge badge-light">{this.state.decomTicketsCount}</span></button>
                    <button onClick={this.getConfigurationOptions} className="btn btn-primary mr-3">Config Options</button>
                </div>
                <div className="fluid-container">
                    {primaryView}
                </div>
            </React.Fragment>
        )
    }
}

class DomainValidations extends Component {
    constructor(props){
        super(props);
        this.state = {domainValidations: this.props.domainValidations}
        this.validate = this.validate.bind(this)
        this.invalidate = this.invalidate.bind(this)
    }
    async validate(user_id){
        let response = await fetch("admins/validate", {
            method: "POST",
            headers: {"Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: JSON.stringify({
                user_id: user_id
            })
        });
        if (response.ok){
            let responseJSON = await response.json()
            if (responseJSON.status == "success") {
                console.log("Success")
                this.props.removeUserSuccessfulResponse(user_id)
                this.props.domainValidationsMinusEqualOne
            } else {
                console.log("Error")
            }
        }
    }
    async invalidate(user_id){
        let response = await fetch("admins/invalidate", {
            method: "POST",
            headers: {"Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: JSON.stringify({
                user_id: user_id
            })
        });
        if (response.ok){
            let responseJSON = await response.json()
            if (responseJSON.status == "success") {
                this.props.removeUserSuccessfulResponse(user_id)
                this.props.domainValidationsMinusEqualOne()
            } else {
                console.log("Error")
            }
        }
    }
    render(){
        let listItems;
        if(this.props.domainValidations.length != []){
            listItems = this.props.domainValidations.map((user) => 
                <tr className="mb-1 mt-1" key={user.id}>
                    <th className="col-sm-12" scope="row">{user.domain}</th>
                    <td><button onClick={() => this.validate(user.id)} className="btn btn-success">Validate</button></td>
                    <td><button onClick={() => this.invalidate(user.id)} className="btn btn-warning">Invalidate</button></td>
                </tr>
            );
        }else{
            listItems = <tr><th>None Rendered</th></tr>
        }
        return(
            <div className="col-sm-10 offset-md-1 mt-3">
                <table className="table table-striped"><tbody>{listItems}</tbody></table>
            </div>
        )
    }
}

class BuildTickets extends Component {
    constructor(props){
        super(props);
        this.state = {buildTickets: this.props.buildTickets}
        this.markComplete = this.markComplete.bind(this)
    }
    async markComplete(user_id){
        let response = await fetch("admins/mark_build_complete", {
            method: "POST",
            headers: {"Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: JSON.stringify({
                user_id: user_id
            })
        });
        if (response.ok){
            let responseJSON = await response.json()
            if (responseJSON.status == "success") {
                this.props.removeCompletedBuild(user_id)
                this.props.buildCompleteMinusEqualOne()
            } else {
                console.log("Error")
            }
        }
    }
    render(){
        let listItems;
        console.log(this.props.buildTickets)
        if(this.props.buildTickets.length != 0){
            listItems = this.props.buildTickets.map((user) => 
                <tr className="mb-1 mt-1" key={user.id}>
                    <th className="col-sm-12" scope="row">{user.domain}</th>
                    <td><button onClick={() => this.markComplete(user.id)} className="btn btn-success">Mark This Build Complete</button></td>
                </tr>
            );
        }else{
            listItems = <tr className="mb-1 mt-1"><td>None Rendered</td></tr>
        }
        return(
            <div className="col-sm-10 offset-md-1 mt-3">
                <table className="table table-striped"><tbody>{listItems}</tbody></table>
            </div>
        )
    }
}

class DecomTickets extends Component {
    constructor(props){
        super(props);
        this.state = {decomTickets: this.props.decomTickets}
        this.markDecom = this.markDecom.bind(this)
    }
    async markDecom(user_id){
        let response = await fetch("admins/mark_decom_complete", {
            method: "POST",
            headers: {"Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: JSON.stringify({
                user_id: user_id
            })
        });
        if (response.ok){
            let responseJSON = await response.json()
            if (responseJSON.status == "success") {
                this.props.removeCompletedDecom(user_id)
                this.props.decomCompleteMinusEqualOne()
            } else if(responseJSON.status == "notification"){
                this.props.removeCompletedDecom(user_id)
                this.props.decomCompleteMinusEqualOne()
            }else{
                console.log("Error!")
            }
        }
    }
    render(){
        let listItems;
        //console.log(this.props.decomTickets)
        if(this.props.decomTickets.length != 0){
            listItems = this.props.decomTickets.map((user) => 
                <tr className="mb-1 mt-1" key={user.id}>
                    <th className="col-sm-12" scope="row">{user.domain}</th>
                    <td><button onClick={() => this.markDecom(user.id)} className="btn btn-success">Mark This Decom Complete</button></td>
                </tr>
            );
        }else{
            listItems = <tr className="mb-1 mt-1"><td>None Rendered</td></tr>
        }
        return(
            <div className="col-sm-10 offset-md-1 mt-3">
                <table className="table table-striped"><tbody>{listItems}</tbody></table>
            </div>
        )
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const node = document.getElementById('AdminDashboard')
    const data = JSON.parse(node.getAttribute('data'))
    ReactDOM.render(
      <AdminDashboard domainValidationsCount={data.domainValidationsCount} buildTicketsCount={data.buildTicketsCount} supportTicketsCount={data.supportTicketsCount} decomTickets={data.decomTickets} decomTicketsCount={data.decomTicketsCount}/>,
      node.appendChild(document.createElement('div')),
    )
})