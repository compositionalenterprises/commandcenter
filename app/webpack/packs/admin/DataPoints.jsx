import React from 'react'
import ReactDOM from 'react-dom'

class DataPoints extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loadedData: false
        }
        this.checkStatus = this.checkStatus.bind(this)
    }
    componentDidMount(){
        fetch("datapoints/environment")
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
                environments: result
            });
            this.checkStatus()
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            this.setState({
            });
          }
        )
        fetch("datapoints/instance")
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
                instances: result
            });
            this.checkStatus()
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            this.setState({
            });
          }
        )
        fetch("datapoints/migration")
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
                migrations: result
            });
            this.checkStatus()
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            this.setState({
            });
          }
        )
    }
    checkStatus(){
        if(
            this.state.instances != null && 
            this.state.migrations != null &&
            this.state.environments != null
        ){
            this.setState({
                loadedData: true
            })
        }
    }
    render(){
        if (this.state.loadedData == false){
            content = <div className="admin-loading-dots">
                <span></span>
                <span></span>
                <span></span>
            </div>
        } else {
            content = <DataPointTable 
                instances={this.state.instances}
                environments={this.state.environments}
                migrations={this.state.migrations}
            />
        }
        return(
            <React.Fragment>
                {content}
            </React.Fragment>
        )
    }
}

class DataPointTable extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            instances: this.props.instances,
            environments: this.props.environments,
            migrations: this.props.migrations
        }
    }
    render(){
        let instance_data = Object.keys(this.state.instances).map((job_status) =>
            <div className="col-sm-10 offset-md-1 mt-3">
            <table className="table table-striped">
                <thead><tr><td><h3>Create Instance {job_status}</h3></td></tr></thead>
                <tbody>
                {
                    this.state.instances[job_status].map((job) =>
                        <tr className="mb-1 mt-1" key={job.id}><th className="col-sm-2" scope="row" style={{color: "orange"}}>{job.argstring}</th></tr>
                    )
                }
            </tbody>
            </table>
            </div>
        );
        let environment_data = Object.keys(this.state.environments).map((job_status) =>
        <div className="col-sm-10 offset-md-1 mt-3">
        <table className="table table-striped">
            <thead><tr><td><h3>Create Environment {job_status}</h3></td></tr></thead>
            <tbody>
            {
                this.state.environments[job_status].map((job) =>
                    <tr className="mb-1 mt-1" key={job.id}><th className="col-sm-2" scope="row" style={{color: "orange"}}>{job.argstring}</th></tr>
                )
            }
        </tbody>
        </table>
        </div>
        );
        let migration_data = Object.keys(this.state.migrations).map((job_status) =>
        <div className="col-sm-10 offset-md-1 mt-3">
        <table className="table table-striped">
            <thead><tr><td><h3>Migration Job {job_status}</h3></td></tr></thead>
            <tbody>
            {
                this.state.migrations[job_status].map((job) =>
                    <tr className="mb-1 mt-1" key={job.id}><th className="col-sm-2" scope="row" style={{color: "orange"}}>{job.argstring}</th></tr>
                )
            }
        </tbody>
        </table>
        </div>
        );
        return(
            <React.Fragment>
                {instance_data}
                {environment_data}
                {migration_data}
            </React.Fragment>
        )
    }
}

export default DataPoints

document.addEventListener('DOMContentLoaded', () => {
    const node = document.getElementById('DataPoints')
    const data = JSON.parse(node.getAttribute('data'))
    ReactDOM.render(
      <DataPoints />,
      node.appendChild(document.createElement('div')),
    )
})
