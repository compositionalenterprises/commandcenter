import React from 'react'
import ReactDOM from 'react-dom';

class CenterView extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            loadedData: false,
            subscriptions: null,
            environments: null,
            servers: null,
            domains: null,
            customer_states: null
        }
        this.checkStatus = this.checkStatus.bind(this)
    }
    componentDidMount(){
      fetch("centerview/subscriptions")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            subscriptions: result.subscriptions
          });
          this.checkStatus()
          // console.log(this.state.subscriptions)
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
          });
        }
      )
      fetch("centerview/environments")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            environments: result.environments
          });
          this.checkStatus()
          // console.log(this.state.environments)
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
          });
        }
      )
      fetch("centerview/servers")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            servers: result.servers
          });
          this.checkStatus()
          // console.log(this.state.servers)
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
          });
        }
      )
      fetch("centerview/domains")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            domains: result.domains
          });
          this.checkStatus()
          // console.log(this.state.domains)
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
          });
        }
      )
      fetch("centerview/states")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            customer_states: result
          });
          this.checkStatus()
          // console.log(this.state.domains)
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
          });
        }
      )
    }
    checkStatus(){
        if(
            this.state.subscriptions != null && 
            this.state.environments != null &&
            this.state.servers != null &&
            this.state.domains != null &&
            this.state.customer_states != null
        ){
            this.setState({
                loadedData: true
            })
        }
    }
    render(){
        if (this.state.loadedData == false){
            content = <div className="admin-loading-dots">
                <span></span>
                <span></span>
                <span></span>
            </div>
        } else {
            content = <CenterViewTable 
                subscriptions={this.state.subscriptions}
                environments={this.state.environments}
                servers={this.state.servers}
                domains={this.state.domains}
                customer_states={this.state.customer_states}
            />
        }
        return(
            <React.Fragment>
                {content}
            </React.Fragment>
        )
    }
}

class CenterViewTable extends React.Component{
  constructor(props){
      super(props);
  }
  componentDidMount(){
      console.log()
  }

  render(){
    let servers_with_offline_or_no_payment = this.props.customer_states.servers_with_offline_or_no_payment.map((server) => 
      <tr className="mb-1 mt-1" key={server}><th className="col-sm-2" scope="row" style={{color: "orange"}}>{server}</th></tr>
    );
    let matched_users_with_valid_payment = this.props.customer_states.matched_users_with_valid_payment.map((user) => 
      <tr className="mb-1 mt-1" key={user.id}><th className="col-sm-2" scope="row" style={{color: "green"}}>{user.domain}</th></tr>
    );
    let misnamed_droplets = this.props.customer_states.misnamed_droplets.map((server) => 
      <tr className="mb-1 mt-1" key={server}><th className="col-sm-2" scope="row" style={{color: "orange"}}>{server}</th></tr>
    );
    let matched_users_invalid_payment = this.props.customer_states.matched_users_invalid_payment.map((user) =>
      <tr className="mb-1 mt-1" key={user.id}><th className="col-sm-2" scope="row">{user.domain}</th></tr>
    );
    let subscription_no_environment = this.props.customer_states.subscription_no_environment.map((user) => 
      <tr className="mb-1 mt-1" key={user}><th className="col-sm-2" scope="row" style={{color: "red"}}>{user}</th></tr>
    );
    let environment_no_instance = this.props.customer_states.environment_no_instance.map((user1) => 
      <tr className="mb-1 mt-1" key={user1}><th className="col-sm-2" scope="row" style={{color: "orange"}}>{user1}</th></tr>
    );
    return(
      <React.Fragment>

        <div className="col-sm-10 offset-md-1 mt-3">
          <table className="table table-striped">
            <thead><tr><td><h3>Misnamed Droplets that Need Immediete Attnetion</h3></td></tr></thead>
            <tbody>{misnamed_droplets}</tbody>
          </table>
        </div>
        <div className="col-sm-10 offset-md-1 mt-3">
          <table className="table table-striped">
            <thead><tr><td><h3>Servers with offline or no paymnet</h3></td></tr></thead>
            <tbody>{servers_with_offline_or_no_payment}</tbody>
          </table>
        </div>
        <div className="col-sm-10 offset-md-1 mt-3">
          <table className="table table-striped">
            <thead><tr><td><h3>Users with Invalid Payment</h3></td></tr></thead>
            <tbody>{matched_users_invalid_payment}</tbody>
          </table>
        </div>
        <div className="col-sm-10 offset-md-1 mt-3">
          <table className="table table-striped">
            <thead><tr><td><h3>Valid Subscriptions with no Environment</h3></td></tr></thead>
            <tbody>{subscription_no_environment}</tbody>
          </table>
        </div>
        <div className="col-sm-10 offset-md-1 mt-3">
          <table className="table table-striped">
            <thead><tr><td><h3>Gitlab Environments with no DO Instance</h3></td></tr></thead>
            <tbody>{environment_no_instance}</tbody>
          </table>
        </div>
        <div className="col-sm-10 offset-md-1 mt-3">
          <table className="table table-striped">
          <thead><tr><td><h3>Matched Domains with Valid Payment</h3></td></tr></thead>
            <tbody>{matched_users_with_valid_payment}</tbody>
          </table>
        </div>
      </React.Fragment>
    )
  }
}

export default CenterView

document.addEventListener('DOMContentLoaded', () => {
    const node = document.getElementById('CenterView')
    const data = JSON.parse(node.getAttribute('data'))
    ReactDOM.render(
      <CenterView />,
      node.appendChild(document.createElement('div')),
    )
})