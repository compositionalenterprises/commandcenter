import React from 'react'

class RundeckConfigurationOptions extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            options: this.props.options
        }
        this.changeValue = this.changeValue.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }
    handleInputChange(e){
        this.setState({[e.target.id]: e.target.value})
    }
    async changeValue(name){
        let response = await fetch("rundeck/options", {
            method: "PUT",
            headers: {"Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: JSON.stringify({
                name: name,
                value: this.state[name],
            })
        });
        if (response.ok){
            let responseJSON = await response.json()
            if (responseJSON.status == "success") {
                console.log("Success")
            } else {
                console.log("Error")
            }
        }
    }
    render(){
        let listItems;
        if(this.props.options.length != 0){
            listItems = this.props.options.map((option) =>
                <tr className="mb-1 mt-1" key={option.name}>
                    <th className="col-sm-2" scope="row">{option.name}</th>
                    <th className="form-group"><input onChange={this.handleInputChange} type="text" className="col-sm-8 form-control" placeholder={option.value} id={option.name}></input></th>
                    <td><button onClick={() => this.changeValue(option.name)} className="btn btn-success">Update Value</button></td>
                </tr>
            );
        }else{
            listItems = <tr className="mb-1 mt-1"><td>None Rendered</td></tr>
        }
        return(
            <div className="col-sm-10 offset-md-1 mt-3">
                <table className="table table-striped"><tbody>{listItems}</tbody></table>
            </div>
        )
    }
}

export default RundeckConfigurationOptions