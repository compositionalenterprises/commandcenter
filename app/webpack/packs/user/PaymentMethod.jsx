import React from "react"
import {Elements, StripeProvider} from 'react-stripe-elements';
import CheckoutForm from "./CheckoutForm";
import MyImage from 'powered-by-stripe2.svg'

class PaymentMethod extends React.Component {
  constructor(props){
    super(props);
    this.state = {isShown: false, hasValidPayment: this.props.hasValidPayment};
    this.handleClick = this.handleClick.bind(this);
    this.hasValidPaymentHandlerPM = this.hasValidPaymentHandlerPM.bind(this);
  }

  handleClick(e){
    this.setState({
      isShown: !this.state.isShown,
    })
  }
  hasValidPaymentHandlerPM(e){
    this.setState({hasValidPayment: e.hasValidPayment})
    this.props.handlePaymentUpdateToAccInfo(this.state)
  }
  render () {
    const isShown = this.state.isShown;
    const hasValidPayment = this.state.hasValidPayment;
    let form, button;
    if(isShown){
      form = <PaymentForm stripeAPIKey={this.props.stripeAPIKey} hasValidPayment={this.state.hasValidPayment} hasValidPaymentHandlerPM={this.hasValidPaymentHandlerPM}/>
    }
    if(hasValidPayment){
      button = (
        <button onClick={(e) => this.handleClick(e)} className="btn btn-success">
          Update Payment Method
        </button>
      )
    }else{
      button = (
        <button onClick={(e) => this.handleClick(e)} className="btn btn-warning">
          Add a Payment Method
        </button>
      )
    }
    return (
      <React.Fragment>
        <div className="d-flex flex-row mt-2">
          {button}
        </div>
        {form}
      </React.Fragment>
    );
  }
}

class PaymentForm extends React.Component {
  constructor(props){
    super(props);
    this.state = {hasValidPayment: this.props.hasValidPayment};
    this.handleHasValidPaymentPF = this.handleHasValidPaymentPF.bind(this)
  }

  handleHasValidPaymentPF(e){
    this.setState({hasValidPayment: e.hasValidPayment})
    this.props.hasValidPaymentHandlerPM(e)
  }
  render () {
    // May want to add some form fields for first & last name and billing address
    return(
      <React.Fragment>
        <StripeProvider apiKey={this.props.stripeAPIKey}>
          <div className="card col-sm-4 pb-2 pt-2 mt-2 rounded bg-white">
            <Elements>
              <CheckoutForm hasValidPayment={this.state.hasValidPayment} hasValidPaymentHandler={this.handleHasValidPaymentPF} />
            </Elements>
            <img style={{width: "10rem", marginTop: ".5rem", marginLeft: "-2rem", height: "2rem"}} className="img-fluid " src={"/commandcenter" + MyImage} />
          </div>
        </StripeProvider>
      </React.Fragment>
    )
  }
}




export default PaymentMethod

