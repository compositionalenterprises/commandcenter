import React from 'react'

class DomainSetup extends React.Component {
    constructor(props){
        super(props);
        this.state = {isShown: false}
    }
    handleClick(e){
        this.setState({isShown: !this.state.isShown})
    }
    render(){
        let button, info;
        const hasValidSubdomain = true
        if(this.state.isShown){
            info = <div>
                <table className="table table-sm">
                <thead>
                    <tr>
                    <th scope="col">Record Type</th>
                    <th scope="col">Host</th>
                    <th scope="col">Value</th>
                    <th scope="col">TTL</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th scope="row">A</th>
                    <td>caas</td>
                    <td>IP.IP.IP.IP</td>
                    <td>Automatic</td>
                    </tr>
                </tbody>
                </table>
            </div>
        }
        if(hasValidSubdomain){
            button = (
                <button onClick={(e) => this.handleClick(e)} className="btn btn-secondary">
                    Setup Domain on Your Side
                </button>
            )
        }
        return (
            <React.Fragment>
                <div className="d-flex flex-row mt-2">
                    {button}
                </div>
                {info}
            </React.Fragment>
        )
    }
}

export default DomainSetup;

