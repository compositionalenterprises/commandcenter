import React from 'react'
import "core-js"

class PersonalInformation extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            hasValidPersonalInformation: false, 
            personalInformation: this.props.personalInformation,
            isShown: false
        }
        this.handleClick = this.handleClick.bind(this);
        this.handlePersonalInformationUpdate = this.handlePersonalInformationUpdate.bind(this)
    }
    componentDidMount(){
        this.setState({hasValidPersonalInformation: this.hasValidPICheck()})
    }
    handleClick(e){
        this.setState({isShown: !this.state.isShown})
    }
    handlePersonalInformationUpdate(state){
        this.setState({
            hasValidPersonalInformation: state.hasValidPI,
            personalInformation: {
                first_name: state.first_name,
                last_name: state.last_name,
                address: state.address,
                address2: state.address2,
                city: state.city,
                zip_code: state.zip_code,
                state: state.state,
                domain: state.domain
            }
        })
    }
    hasValidPICheck(){
        if(!( 
            this.state.personalInformation.domain == null ||
            this.state.personalInformation.domain == ""
        )){
            return true
        }else{
            return false
        }
    }

    render() {
        const isShown = this.state.isShown
        const hasValidPersonalInformation = this.state.hasValidPersonalInformation
        let button, form;
        if(isShown){
            form = <PersonalInformationForm 
                hasValidPersonalInformation={this.state.hasValidPersonalInformation}
                personalInformation={this.state.personalInformation} 
                handlePersonalInformationUpdate={this.handlePersonalInformationUpdate}
                handleDomainUpdateToAccInfo={this.props.handleDomainUpdateToAccInfo}
            />
        }
        if(hasValidPersonalInformation){
            button = (
              <button onClick={(e) => this.handleClick(e)} className="btn btn-success">
                Update Information
              </button>
            )
          }else{
            button = (
              <button onClick={(e) => this.handleClick(e)} className="btn btn-warning">
                Add More Information <small>(Domain Required)</small>
              </button>
              
            )
        }
        return (
            <React.Fragment>
                <div className="d-flex flex-row mt-2">
                    {button}
                </div>
                {form}
            </React.Fragment>
        )
    }
}

class PersonalInformationForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            hasValidPI: this.props.hasValidPersonalInformation,
            info_updated: null,
            first_name: this.props.personalInformation.first_name,
            last_name: this.props.personalInformation.last_name,
            address: this.props.personalInformation.address,
            address2: this.props.personalInformation.address2,
            city: this.props.personalInformation.city,
            zip_code: this.props.personalInformation.zip_code,
            domain: this.props.personalInformation.domain,
            domain_locked: this.props.personalInformation.domain_locked
        }
        this.editInformation = this.editInformation.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.hasValidPICheck = this.hasValidPICheck.bind(this)
    }
    handleInputChange(e){
        this.setState({[e.target.id]: e.target.value})
    }
    handlePersonalInformationUpdate(state){
        this.props.handlePersonalInformationUpdate(state)
    }
    hasValidPICheck(){
        if(!(
            this.state.domain == null ||
            this.state.domain == "" 
        )){
            return true
        }else{
            return false
        }
    }
    async editInformation(e){
        let bodey = null
        if(this.state.domain_locked){
            bodey =  JSON.stringify({
                firstname: this.state.first_name,
                lastname: this.state.last_name,
                address: this.state.address,
                address2: this.state.address2,
                city: this.state.city,
                zip_code: this.state.zip_code,
            })
        }else{
            bodey =  JSON.stringify({
                firstname: this.state.first_name,
                lastname: this.state.last_name,
                address: this.state.address,
                address2: this.state.address2,
                city: this.state.city,
                zip_code: this.state.zip_code,
                domain: this.state.domain + ".ourcompose.com"
            })
        }
        let response = await fetch("account", {
            method: "PUT",
            headers: {"Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: bodey
        });
        if (response.ok){
            let responseJSON = await response.json()
            if (responseJSON.status == "success") {
                this.setState({info_updated: true, hasValidPI: this.hasValidPICheck()});
                this.handlePersonalInformationUpdate(this.state)
                if (this.hasValidPICheck()){
                    this.props.handleDomainUpdateToAccInfo({hasValidDomain: true})
                }
                // React doesn't like this; Claims it is a memory leak
                setTimeout(() => {
                    this.setState({
                      info_updated: null
                    });
                }, 5000)
            } else {
              // Update the state to reflect the error
              // Render an error based on the message
              this.setState({info_updated: false, hasValidPI: this.hasValidPICheck(), errors: responseJSON.errors});
            }
        }
    }
    render() {
        let genInfoSubmitButton, infoUpdateNotice, domainForm;
        if(this.hasValidPICheck()){
            genInfoSubmitButton = <button className="btn btn-light col-md-8 mb-4" style={{border: ".25px solid"}} onClick={(e) => this.editInformation(e)}>Update Information</button>  
        } else {
            genInfoSubmitButton = <button className="btn btn-light col-md-8 mb-4" style={{border: ".25px solid"}} onClick={(e) => this.editInformation(e)}>Add Information</button>  
        }
        if(this.state.info_updated){
            infoUpdateNotice = <div>Thank You! You have <span className="badge badge-success">successfully</span> updated your account! <small>(I will disappear!)</small></div>
        }else if(this.state.info_updated == false){
            infoUpdateNotice = <div>Error <span className="badge badge-danger">unsuccessfully</span> updated your account! <small>You will need to retry!</small></div>
        }
        if(this.state.domain_locked){
            let fulldomain;
            if(this.state.domain == ''){
                fulldomain = ''
            }else{
                fulldomain = this.state.domain
            }
            domainForm = (<fieldset disabled><div className="form-group">
                <label htmlFor="inputDomain">Domain (Required)</label>
                <input type="text" className="form-control" id="domain" value={fulldomain} onChange={this.handleInputChange} />
                <small id="domainHelp" className="form-text text-muted">We need your domain to properly point the services to the correct place.</small>
            </div></fieldset>)
        }else{
            domainForm = (<div className="form-group">
                <label htmlFor="inputDomain">Domain (Required)</label>
                <div className="input-group">
                    <input type="text" className="form-control" id="domain" value={this.state.domain || ''} onChange={this.handleInputChange} />
                    <div className="input-group-append">
                        <span className="input-group-text" data-toggle="tooltip" data-placement="top" title="Right now we only support subdomains. We are working hard to support custom domains!">
                            .ourcompose.com
                        </span>
                    </div>
                </div>
                <small id="domainHelp" className="form-text text-muted">We need your domain to properly point the services to the correct place.</small>
            </div>)
        }
        return (
            <div className="mt-2 fluid-container">
                <div className="card col-sm-4 pt-2 pb-2">
                    <div className="form-row">
                        <div className="form-group col-md-6">
                        <label htmlFor="firstName">First Name</label>
                        <input type="text" className="form-control" id="first_name" value={this.state.first_name || ''} onChange={this.handleInputChange}/>
                        </div>
                        <div className="form-group col-md-6">
                        <label htmlFor="lastName">Last Name</label>
                        <input type="text" className="form-control" id="last_name" value={this.state.last_name || ''} onChange={this.handleInputChange}/>
                        </div>
                    </div>
                <br />
                <div className="form-group">
                    <label htmlFor="inputAddress">Address</label>
                    <input type="text" className="form-control" id="address" value={this.state.address || ''} onChange={this.handleInputChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="inputAddress2">Address 2</label>
                    <input type="text" className="form-control" id="address2" value={this.state.address2 || ''} onChange={this.handleInputChange}/>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-6">
                    <label htmlFor="inputCity">City</label>
                    <input type="text" className="form-control" id="city" value={this.state.city || ''} onChange={this.handleInputChange}/>
                    </div>
                    <div className="form-group col-md-4">
                    <label htmlFor="inputZip">Zip</label>
                    <input type="text" className="form-control" id="zip_code" value={this.state.zip_code || ''} onChange={this.handleInputChange}/>
                    </div>
                </div>
                {domainForm}
                {genInfoSubmitButton}
                {infoUpdateNotice}
                </div>
            </div>
        )
    }
}


export default PersonalInformation;
