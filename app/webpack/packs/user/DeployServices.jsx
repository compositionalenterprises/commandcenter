import React from 'react'
import "core-js"

class DeployServices extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isShown: false,
            deploy: null,
            hasValidDomain: this.props.hasValidDomain,
            hasValidPayment: this.props.hasValidPayment,
            hasValidPlan: this.props.hasValidPlan,
            validDeploymentState: this.props.hasValidPayment && this.props.hasValidPlan && this.props.hasValidDomain,
            deploymentStatus: this.props.deploymentStatus,
            deploymentStatusMessage: this.props.deploymentStatusMessage,
            error: null,
            personalInformation: this.props.personalInformation,
            password_confirmation: null,
            passwordStatus: null
        }
        this.deployServices = this.deployServices.bind(this)
        this.getPassword = this.getPassword.bind(this)
    }
    getPassword(e){
        e.preventDefault();
        this.setState({
            password_confirmation: e.target.value
        })
    }
    async deployServices(e){
        e.preventDefault();
        let response = await fetch("services/deploy", {
            method: "POST",
            headers: {"Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: JSON.stringify({
                start: true,
                password_confirmation: this.state.password_confirmation
            })
        });
        if (response.ok){
            let responseJSON = await response.json()
            if (responseJSON.status == "success") {
                this.setState({deploymentStatus: responseJSON.message, isShown: true});
                this.props.handleDeploymentStatusMessage({deploymentStatus: "deployInProgress"})
                // Update Deploy Message Here
            }else if (responseJSON.status == "error" && responseJSON.message == "wrong-password"){
                this.setState({passwordStatus: "failed"})
            } else {
                this.setState({deploymentStatus: responseJSON.message, isShown: true});
                this.setState({error: true})
            }
        }
    }
    componentDidUpdate(){
        // console.log("Deploy Services")
        // console.log(this.props.deploymentStatus)    
    }
    render() {
        let button, statusInfo, linkToServices, passwordform, passwordStatus;
        statusInfo = <div className="badge badge-secondary">
            {this.props.deploymentStatusMessage}
        </div>
        if (this.state.passwordStatus == "failed"){
            passwordStatus = <div className="badge badge-danger mr-1">
                Failed Password! Please retry!
            </div>
        }
        if (this.state.error == true){
            button = <button className="btn btn-danger" onClick={(e) => this.deployServices(e)}>Error Deploying Services</button>
            statusInfo = <div className="badge badge-secondary">Please Refresh the Page and Try again or Reach out to support@ourcompose.com</div>
        }else if(this.props.hasValidPayment && this.props.hasValidPlan && this.props.hasValidDomain && this.props.deploymentStatus == "pendingDeploy"){
            button = <button className="btn btn-primary" onClick={(e) => this.deployServices(e)}>Deploy Services (please fill out password form below to confirm account)</button>
            passwordform = <form>
                <input className="form-control form-control-sm col-sm-2 mt-2" type="password" placeholder="Password" autoComplete="new-password" onKeyUp={(e) => this.getPassword(e)}></input>
            </form>
        }else if(this.props.deploymentStatus == "pendingUnsubscribe"){
            button = <button className="btn btn-success disabled">Services Currently Unavailable</button>
        }else if(this.props.deploymentStatus == "deployComplete"){
            button = <button className="btn btn-success disabled">Services Deployed</button>
        }else{
            button = <button className="btn btn-warning disabled">Deploy Services</button>
        }
        if(this.props.deploymentStatus == "deployComplete"){
            let link = "http://" + this.props.personalInformation.domain + ".ourcompose.com"
            linkToServices = <div>
                <a href={link} target="_blank" className="btn btn-primary mt-2">Go To Your Services!</a>
            </div>
        }
        return (
            <React.Fragment>
                <div className="d-flex flex-row mt-2">
                    {button}
                </div>
                {passwordform}
                {passwordStatus}
                {statusInfo}
                {linkToServices}
            </React.Fragment>
        )
    }
}

export default DeployServices
