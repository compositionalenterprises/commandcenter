import React from 'react'
import "core-js"

class ApplicationSelection extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            userApplicationsSelected: null,
            userServicesDeployed: null,
            applications: [],
            activeApplications: [],
            isShown: false,
            selectedPlans: this.props.selectedPlans,
        }
        this.handleClick = this.handleClick.bind(this)
        this.handleCheck = this.handleCheck.bind(this)
        this.checkActive = this.checkActive.bind(this)
        this.handleUpdateServices = this.handleUpdateServices.bind(this)
    }
    componentDidMount(){
        fetch('account/applications')
        .then(response => response.json())
        .then(data => this.setState({
            applications: data.applications, 
            activeApplications: data.currentlySelectedApplications,
            userApplicationsSelected: data.userApplicationsSelected,
            userServicesDeployed: data.userServicesDeployed
        }));
    }
    componentDidUpdate(){
    }

    handleClick(e){
        this.setState({
            isShown: !this.state.isShown,
        })
    }
    handleCheck(e){
        if(e.target.checked){
            var newApplicationList = [...this.state.activeApplications, e.target.id]
            this.setState({activeApplications: newApplicationList})
        }else{
            var newApplicationList = [];
            for(var i = 0; i < this.state.activeApplications.length; i++){
                if(this.state.activeApplications[i] != e.target.id){
                    newApplicationList.push(this.state.activeApplications[i])
                }
            }
            this.setState({activeApplications: newApplicationList})
        }
    }
    async handleUpdateServices(e){
        e.preventDefault();
        let response = await fetch("account/applications", {
            method: "POST",
            headers: {"Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: JSON.stringify({
                applications: this.state.activeApplications
            })
        });
        if (response.ok){
            let responseJSON = await response.json()
            if (responseJSON.status == "success") {
                this.setState({userApplicationsSelected: true})
            } else {
                console.log("Error")
            }
        }
    }
    checkActive(appName){
        return this.state.activeApplications.includes(appName)
    }
    render(){
        let button, form, otherMessage;
        // console.log("User Services Deployed:")
        // console.log(this.state.userServicesDeployed)
        if(this.state.isShown){
            if(this.state.userServicesDeployed == true){
                otherMessage = null;
                if (this.state.activeApplications.length == 0){
                    otherMessage = <div>Default Services have been Deployed!</div>
                    this.setState({activeApplications: ["wordpress", "nextcloud", "bitwarden"]})
                }
                form = 
                <React.Fragment>
                <div className="mt-2">
                    <div className="card" style={{width: 18 + "rem"}}>
                        <div className="card-body">
                            <h5 className="card-title">Selected Services</h5>
                            <h6 className="card-subtitle mb-2 text-muted"></h6>
                            {this.state.activeApplications.map((application) =>
                                <div className="form-check" key={application}>
                                <input className="form-check-input" type="checkbox" value="" id={application} checked="true" disabled/>
                                <label className="form-check-label">
                                    {application}
                                </label>
                                </div>
                            )}
                            {otherMessage}
                        </div>
                    </div>
                </div>
                </React.Fragment>
            }else{
                if(!this.state.userApplicationsSelected){
                    form = 
                        <React.Fragment>
                        <div className="mt-2">
                            <div className="card" style={{width: 18 + "rem"}}>
                                <div className="card-body">
                                    <h5 className="card-title">Select Services</h5>
                                    <h6 className="card-subtitle mb-2 text-muted">Please note applications will be limited based on your instance size.</h6>
                                    {this.state.applications.map((application) =>
                                        <div className="form-check" key={application}>
                                        <input className="form-check-input" type="checkbox" value="" id={application} checked={this.checkActive(application)} onChange={this.handleCheck} />
                                        <label className="form-check-label">
                                            {application}
                                        </label>
                                        </div>
                                    )}
                                    <button className="mt-2 btn btn-primary" onClick={this.handleUpdateServices}>
                                        Select Services
                                    </button>
                                </div>
                            </div>
                        </div>
                        </React.Fragment>
                }else{
                    form = 
                        <React.Fragment>
                        <div className="mt-2">
                            <div className="card" style={{width: 18 + "rem"}}>
                                <div className="card-body">
                                    <h5 className="card-title">Selected Services</h5>
                                    <h6 className="card-subtitle mb-2 text-muted">Please note applications will be limited based on your instance size.</h6>
                                    {this.state.applications.map((application) =>
                                        <div className="form-check" key={application}>
                                        <input className="form-check-input" type="checkbox" value="" id={application} checked={this.checkActive(application)} onChange={this.handleCheck} />
                                        <label className="form-check-label">
                                            {application}
                                        </label>
                                        </div>
                                    )}
                                    <button className="mt-2 btn btn-primary" onClick={this.handleUpdateServices}>
                                        Update Services
                                    </button>
                                </div>
                            </div>
                        </div>
                        </React.Fragment>
                }
            }
        }
        if(this.state.userApplicationsSelected == true){
            button = (
                <button onClick={(e) => this.handleClick(e)} className="btn btn-success">
                    View Applications
                </button>
            )
        }else{
            button = (
                <button onClick={(e) => this.handleClick(e)} className="btn btn-warning">
                    Select Applications
                </button>
            )
        }

        return(
            <React.Fragment>
            <div className="d-flex flex-row mt-2">
                {button}
            </div>
            {form}
        </React.Fragment>
        )
    }

}


export default ApplicationSelection;
