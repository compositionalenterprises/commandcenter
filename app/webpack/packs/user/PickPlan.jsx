import React from 'react'
import "core-js"

class PickPlan extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            hasValidPayment: this.props.hasValidPayment, 
            hasValidPlan: this.props.hasValidPlan, 
            selectedPlans: this.props.selectedPlans, 
            isShown: false,
            deploymentStatus: this.props.deploymentStatus,
            error: null
        }
        this.handleClick = this.handleClick.bind(this);
        this.handleSuccessfulUnsubscribe = this.handleSuccessfulUnsubscribe.bind(this);
        this.handleSuccessfulSubscribe = this.handleSuccessfulSubscribe.bind(this);
    }
    handleClick(e){
        this.setState({
            isShown: !this.state.isShown,
        })
    }
    handleSuccessfulSubscribe(state){
        this.setState({
            hasValidPlan: state.hasValidPlan,
            deploymentStatus: state.deploymentStatus,
            selectedPlans: state.selectedPlans,
            error: state.error
        })
        this.props.handlePlanUpdateToAccInfo({deploymentStatus: state.deploymentStatus, hasValidPlan: state.hasValidPlan, selectedPlans: state.selectedPlans})
    }
    handleSuccessfulUnsubscribe(state){
        this.setState({
            hasValidPlan: state.hasValidPlan,
            deploymentStatus: state.deploymentStatus,
            error: state.error
        })
        this.props.handlePlanUpdateToAccInfo({deploymentStatus: state.deploymentStatus, hasValidPlan: state.hasValidPlan})
    }
    handleError(state){

    }

    // Entire render() method could be refactored..
    render() {
        let form, button;
        if(this.state.isShown){
            if(!this.props.hasValidPayment){
                form = 
                    <React.Fragment>
                    <div className="mt-2">
                        <div className="card" style={{width: 18 + "rem"}}>
                            <div className="card-body">
                                <h5 className="card-title">Please add a Credit Card</h5>
                                <h6 className="card-subtitle mb-2 text-muted"></h6>
                                <p className="card-text">Adding a credit card will allow us to associate a plan with your account.</p>
                                <a href="#" className="card-link btn btn-light" style={{border: ".25px solid"}}>Support</a>
                                <a href="#" className="card-link">More Information</a>
                            </div>
                        </div>
                    </div>
                    </React.Fragment>
            }else{
                form = <SelectPlanForm 
                    selectedPlans={this.state.selectedPlans} 
                    handleSuccessfulUnsubscribe={this.handleSuccessfulUnsubscribe} 
                    handleSuccessfulSubscribe={this.handleSuccessfulSubscribe}
                    deploymentStatus={this.state.deploymentStatus}
                />
            }
        }
        if(this.state.error == true){
            button = (
                <button onClick={(e) => this.handleClick(e)} className="btn btn-danger">
                    Error Updating your Account! <small>(Please Refresh and Try Again or contact support@ourcompose.com)</small>
                </button>
            )
        }else if(this.state.hasValidPlan && this.state.deploymentStatus != "pendingUnsubscribe"){
            button = (
                <button onClick={(e) => this.handleClick(e)} className="btn btn-success">
                    Update Plan
                </button>
            )
        }else{
            button = (
                <button onClick={(e) => this.handleClick(e)} className="btn btn-warning">
                    Select A Plan
                </button>
            )
        }
        return (
            <React.Fragment>
                <div className="d-flex flex-row mt-2">
                    {button}
                </div>
                {form}
            </React.Fragment>
        )
    }
}

class SelectPlanForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            selectedPlans: this.props.selectedPlans, 
            deploymentStatus: this.props.deploymentStatus,
        }
        this.handleSuccessfulSubscribe = this.handleSuccessfulSubscribe.bind(this)
        this.handleSuccessfulUnsubscribe = this.handleSuccessfulUnsubscribe.bind(this)
    }

    // componentDidMount(){
    //     console.log(this.state)
    // }

    handleSuccessfulSubscribe(e){
        this.setState(e)
        this.props.handleSuccessfulSubscribe(this.state)
    }

    handleSuccessfulUnsubscribe(e){
        this.setState(e)
        this.props.handleSuccessfulUnsubscribe(this.state)
    }

    //
    // Minimal Plan Integration
    // Put the below code above the Personal Plan
    //
    // <Card 
    //     planName="minimal" 
    //     handleSuccessfulSubscribe={this.handleSuccessfulSubscribe}
    //     handleSuccessfulUnsubscribe={this.handleSuccessfulUnsubscribe}
    //     selectedPlans={this.state.selectedPlans}
    //     deploymentStatus={this.state.deploymentStatus}
    // />
    //

    render(){
        return (
            <React.Fragment>
            <div className="mt-2">
                <div className="card-deck">
                    <Card 
                        planName="personal" 
                        handleSuccessfulSubscribe={this.handleSuccessfulSubscribe}
                        handleSuccessfulUnsubscribe={this.handleSuccessfulUnsubscribe}
                        selectedPlans={this.state.selectedPlans}
                        deploymentStatus={this.state.deploymentStatus}
                    />
                    <Card 
                        planName="personal_plus" 
                        handleSuccessfulSubscribe={this.handleSuccessfulSubscribe}
                        handleSuccessfulUnsubscribe={this.handleSuccessfulUnsubscribe}
                        selectedPlans={this.state.selectedPlans}
                        deploymentStatus={this.state.deploymentStatus}
                    />
                </div>
            </div>
            </React.Fragment>
        )
    }
}

class Card extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            selectedPlans: this.props.selectedPlans,
            deploymentStatus: this.props.deploymentStatus
        }
    }

    render(){
        let name, price, description;
        switch(this.props.planName){
            case "minimal":
                name = "Minimal"
                price = "$15"
                description = "A basic toolkit made of the application of your choice!"
                break;
            case "personal":
                name = "Personal"
                price = "$30"
                description = "A full set of applications built for 1"
                break;
            case "personal_plus":
                name = "Personal Plus"
                price = "$45"
                description = "Take the personal plan and add some more juice in the likes of RAM, CPU, and Space"
                break;
        }
        return (
            <React.Fragment>
                <div className="card" style={{width: 18 + "rem"}}>
                    <div className="card-body">
                        <h5 className="card-title">{name} Plan</h5>
                        <h6 className="card-subtitle mb-2 text-muted">{price}/Month</h6>
                        <p className="card-text">{description}</p>
                        <Button 
                            handleSuccessfulSubscribe={this.props.handleSuccessfulSubscribe}
                            handleSuccessfulUnsubscribe={this.props.handleSuccessfulUnsubscribe}
                            selectedPlans={this.state.selectedPlans}
                            deploymentStatus={this.state.deploymentStatus}
                            planName={this.props.planName}
                        />
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

class Button extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            checked: false, 
            deploymentStatus: this.props.deploymentStatus,
            selectedPlans: this.props.selectedPlans,
            error: null
        }
        this.handleSuccessfulSubscribe = this.handleSuccessfulSubscribe.bind(this)
        this.handleSuccessfulUnsubscribe = this.handleSuccessfulUnsubscribe.bind(this)
        this.handleCheckboxButton = this.handleCheckboxButton.bind(this)
        this.handleSubscribe = this.handleSubscribe.bind(this)
        this.handleUnsubscribe = this.handleUnsubscribe.bind(this)
    }
    async handleUnsubscribe(e){
        if(this.state.checked == false){
            this.setState({message: "Error! You must check the box above!"})
            return 0
        }
        let response = await fetch("billing/plans", {
            method: "DELETE",
            headers: {"Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: JSON.stringify({
                plan_name: this.props.planName
            })
        });
        if (response.ok){
            let responseJSON = await response.json()
            if (responseJSON.status == "success") {
                this.setState({selectedPlans: [], hasValidPlan: false, deploymentStatus: responseJSON.deploymentStatus});
                this.handleSuccessfulUnsubscribe()
                //$('#close-account-modal').modal('hide')
                $('.modal-backdrop').remove();
            } else {
                this.setState({error: true})
                this.handleSuccessfulUnsubscribe()
                //$('#close-account-modal').modal('hide')
                $('.modal-backdrop').remove();
            }
        }
    }
    async handleSubscribe(e){
        let response = await fetch("billing/plans", {
            method: "POST",
            headers: {"Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            body: (JSON.stringify({
                plan_name: this.props.planName
            }))
        });
        if (response.ok){
            let responseJSON = await response.json()
            if (responseJSON.status == "success") {
                this.setState({selectedPlans: [this.props.planName + "_plan"], hasValidPlan: true, deploymentStatus: responseJSON.deploymentStatus});
                this.handleSuccessfulSubscribe()
            } else {
                this.setState({error: true})
                this.handleSuccessfulUnsubscribe()
            }
        }
    }

    handleCheckboxButton(e){
        this.setState({checked: !this.state.checked})
    }

    handleSuccessfulSubscribe(){
        this.props.handleSuccessfulSubscribe(this.state)
    }

    handleSuccessfulUnsubscribe(){
        this.props.handleSuccessfulUnsubscribe(this.state)
    }

    render(){
        let button, plan_name;
        if(this.state.deploymentStatus == "pendingUnsubscribe"){
            button = (
                <div>
                    <b>Asset being decommissioned</b>
                </div>
            )
        }else if (this.state.selectedPlans.length == 0) {
            button = (
                <div>
                    <div className="card-link btn btn-light" style={{border: ".25px solid"}} onClick={(e) => this.handleSubscribe(e) } id="plan_individual">Select</div>
                    <a href="#" className="card-link">More Information</a>
                </div>
            )
        } else if (this.state.selectedPlans[0] == this.props.planName + "_plan"){
            // Probably need to break down button & modal into another react component
            // This disgusts me
            button = (
                <div>
                    <button type="button" className="btn btn-danger" data-toggle="modal" data-target=".bd-example-modal-sm">Unsubscribe from this Plan</button>
                    <div className="modal fade bd-example-modal-sm" tabIndex="-1" id="close-account-modal" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div className="modal-dialog modal-sm">
                            <div className="modal-content">
                                <div className="modal-header bg-danger text-white">
                                    <h5 className="modal-title" id="exampleModalLabel">Unsubscription Notice</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <div>
                                        Unsubscribing from this plan will immedietly delete all accounts and information stored on any servers.
                                        All backups should be made before unsubscribing. 
                                    </div>
                                    <br />
                                    <div>
                                        To confirm your cancellation please check the box below and then hit the <b>close my account</b> button:
                                    </div>
                                    <div className="form-check mt-2">
                                        <input type="checkbox" className="form-check-input" id="closeAccount" onChange={this.handleCheckboxButton} checked={this.state.checked} />
                                        <label className="form-check-label">I am sure I would like to close my account.</label>
                                    </div>
                                </div>
                                <div className="modal-footer justify-content-start">
                                    <button type="button" className="btn btn-danger d-flex justify-content-start" onClick={this.handleUnsubscribe}>Close My Account</button>
                                </div>
                                {this.state.message}
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else {
            let name;
            switch (this.state.selectedPlans[0]){
                case "minimal_plan":
                    name = "Minimal"
                    break;
                case "personal_plan":
                    name = "Personal"
                    break;
                case "personal_plus_plan":
                    name = "Personal Plus"
                    break;
                default:
                    name = "default?"
                    break;
            }
            button = <div>You have selected the <span className="badge badge-primary">{name} plan!</span></div>
        }
        return(
            <React.Fragment>
                {button}
            </React.Fragment>
        )
    }
}


export default PickPlan;
