import React from "react"
import {injectStripe, CardElement} from 'react-stripe-elements';
import "core-js"

class CheckoutForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {infoUpdated: false, hasValidPayment: this.props.hasValidPayment};
      this.submit = this.submit.bind(this);
      this.handleHasValidPayment = this.handleHasValidPayment.bind(this);
    }

    componentDidMount(){
    }

    handleHasValidPayment(e){
        this.props.hasValidPaymentHandler(e)
    }
  
    async submit(ev) {
      // User clicked submit
      let {token} = await this.props.stripe.createToken({name: "Name"});
      if(token == undefined){
        this.setState({error:true})
      }
      let response = await fetch("billing/customer", {
        method: "POST",
        headers: {"Content-Type": "application/json", 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
        body: JSON.stringify({
          stripeToken: token.id
        })
      });
    
      if (response.ok){
        let responseJSON = await response.json()
        if (responseJSON.status == "success") {
          this.setState({infoUpdated: true, infoUpdatedVal: true, hasValidPayment: true});
          this.handleHasValidPayment(this.state)
        } else {
          this.setState({infoUpdated: true, infoUpdatedVal: false, hasValidPayment: false});
          // Render an error based on the message
        }
      }
    }
    
  
    render() {
      if (this.state.infoUpdated){
        if(this.state.infoUpdatedVal){
          return <div><h5 className="text-dark">Added Your Credit Card Information! Thank You!</h5></div>;
        }else{
          return <div><h5 className="text-dark">Oh No! It looks like we ran into an error processing your card!</h5> <div className="badge badge-secondary">To retry, please re-open this dialog box!</div></div>;
        }
      }
      let button;
      if(this.state.hasValidPayment){
        button = (
            <button onClick={this.submit} className="btn mt-3" style={{border: ".25px solid"}}>Update Credit Card Information</button>
        )
      }else{
        button = (
            <button onClick={this.submit} className="btn mt-3" style={{border: ".25px solid"}}>Add Credit Card</button>
        )
      }
      return (
        <div className="checkout mt-2">
          <CardElement style={style}/>
          {button}
        </div>
      );
    }
  }

  const style = {
    base: {
      color: '#32325d',
      fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
      fontSmoothing: 'antialiased',
      fontSize: '16px',
      '::placeholder': {
        color: '#2d2d53'
      }
    },
    invalid: {
      color: '#fa755a',
      iconColor: '#fa755a'
    }
  };

export default injectStripe(CheckoutForm);
  