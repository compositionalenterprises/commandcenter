import React from 'react'
import ReactDOM from 'react-dom'
import "core-js"
import PersonalInformation from './user/PersonalInformation';
import PaymentMethod from './user/PaymentMethod';
import PickPlan from './user/PickPlan';
import DeployServices from './user/DeployServices';
import DomainSetup from './user/DomainSetup';
import Applications from './user/Applications';

export default class AccountInfo extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            personalInformation: this.props.userInfo.personalInformation,
            hasValidPayment: this.props.userInfo.hasValidPayment,
            hasValidPlan: this.props.userInfo.hasValidPlan,
            hasValidDomain: this.props.userInfo.hasValidDomain,
            selectedPlans: this.props.userInfo.selectedPlans,
            deploymentStatus: this.props.userInfo.deploymentStatus,
        }
        this.handleDomainUpdateToAccInfo = this.handleDomainUpdateToAccInfo.bind(this)
        this.handlePaymentUpdateToAccInfo = this.handlePaymentUpdateToAccInfo.bind(this)
        this.handlePlanUpdateToAccInfo = this.handlePlanUpdateToAccInfo.bind(this)
        this.handleDeployClick = this.handleDeployClick.bind(this)
        this.handleDeploymentStatusMessage = this.handleDeploymentStatusMessage.bind(this)
    }
    handlePaymentUpdateToAccInfo(e){
        this.setState({hasValidPayment: e.hasValidPayment})
        this.handleDeploymentStatusMessage()
    }
    handlePlanUpdateToAccInfo(e){
        this.setState({hasValidPlan: e.hasValidPlan, deploymentStatus: e.deploymentStatus, selectedPlans: e.selectedPlans})
        this.handleDeploymentStatusMessage()
    }
    handleDomainUpdateToAccInfo(e){
        this.setState({hasValidDomain: e.hasValidDomain})
        this.handleDeploymentStatusMessage()
    }
    handleDeployClick(e){
        this.setState({deploymentStatus: e.deploymentStatus})
        this.handleDeploymentStatusMessage()
    }
    handleDeploymentStatusMessage(){
        // console.log(this.state.deploymentStatus)
        if(!this.state.hasValidPlan || !this.state.hasValidPayment){
            this.setState({deploymentStatusMessage: "Please finish setting up your account!"})
        }else if(!this.state.hasValidDomain){
            this.setState({deploymentStatusMessage: "Please wait for us to validate your domain, you will receive an email when ready!"})
        }else if(this.state.deploymentStatus == "deployInProgress"){
            this.setState({deploymentStatusMessage: "Build is currently in progress, we will email you when complete!"})
        }else if(this.state.deploymentStatus == "deployComplete"){
            this.setState({deploymentStatusMessage: "Services are Deployed!"})
        }else if(this.state.deploymentStatus == "pendingUnsubscribe"){
            this.setState({deploymentStatusMessage: "Services are being decommissioned!"})
        }else if(this.state.deploymentStatus == "unsubscribed"){
            this.setState({deploymentStatusMessage: "Services are decommissioned!"})
        }else if(this.state.hasValidDomain && this.state.hasValidPayment && this.state.hasValidPlan){
            this.setState({deploymentStatusMessage:"Ready to Deploy!"})
        }
    }
    componentDidMount(){
        this.handleDeploymentStatusMessage()
    }
    componentDidUpdate(){
        // console.log("Account Info")
        // console.log(this.state)
    }
    render() {
        let needsToBeCompleted;
        if(this.props.hasValidAccount){
            needsToBeCompleted = <h1>Needs to be Completed:</h1>
        }
        return (
            <div>
                {needsToBeCompleted}
                <PersonalInformation 
                    personalInformation={this.state.personalInformation} 
                    handleDomainUpdateToAccInfo={this.handleDomainUpdateToAccInfo}
                />
                <PaymentMethod 
                    stripeAPIKey = {this.props.stripeAPIKey}
                    hasValidPayment={this.state.hasValidPayment}
                    handlePaymentUpdateToAccInfo={this.handlePaymentUpdateToAccInfo}
                />
                <PickPlan 
                    hasValidPayment={this.state.hasValidPayment} 
                    hasValidPlan={this.state.hasValidPlan} 
                    selectedPlans={this.state.selectedPlans}
                    handlePlanUpdateToAccInfo={this.handlePlanUpdateToAccInfo}
                    deploymentStatus={this.state.deploymentStatus}
                />
                <Applications 
                    selectedPlans={this.state.selectedPlans}
                />
                <DeployServices
                    hasValidPayment={this.state.hasValidPayment}
                    hasValidPlan={this.state.hasValidPlan}
                    hasValidDomain={this.state.hasValidDomain}
                    deploymentStatus={this.state.deploymentStatus}
                    deploymentStatusMessage={this.state.deploymentStatusMessage}
                    handleDeploymentStatusMessage={this.handleDeployClick}
                    personalInformation={this.state.personalInformation}
                />
            </div>
            // Hidden, but <DomainSetup /> is currently excluded
        )
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const node = document.getElementById('AccountInfo')
    const data = JSON.parse(node.getAttribute('data-userInfo'))
    const stripeAPIKey = JSON.parse(node.getAttribute('data-stripeAPIKey'))
  
    // console.log(stripeAPIKey.stripe_pub_key)
    ReactDOM.render(
      <AccountInfo userInfo={data} stripeAPIKey={stripeAPIKey.stripe_pub_key} />,
      node.appendChild(document.createElement('div')),
    )
})