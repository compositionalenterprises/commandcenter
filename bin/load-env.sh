#!/bin/bash

#
# Script used to load .env file into environment for dev environment
#


cat ./.env | while read line; do
    export $line
done
