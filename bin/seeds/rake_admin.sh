#!/bin/sh
# rake_admin.sh script
# ENVIRONMENT VARIBLES THAT NEED TO BE SET:
#   CC_ADMIN_CREATE_UPDATE= (string) "CREATE" or "UPDATE"
#   CC_ADMIN_EMAIL= (string) admin email address
#   CC_ADMIN_PASSWORD= (string) admin password
#   CC_ADMIN_SEND_EMAIL_FLAG= (string) "YES" or "NO"


export RAILS_ENV=production
bundle exec rake db:seed:single SEED=admin CREATE_UPDATE=$CC_ADMIN_CREATE_UPDATE EMAIL=$CC_ADMIN_EMAIL PASSWORD=$CC_ADMIN_PASSWORD SEND_EMAIL_FLAG=$CC_ADMIN_SEND_EMAIL_FLAG
