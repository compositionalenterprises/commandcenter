#!/bin/sh
export RAILS_ENV=production
bundle exec rake db:seed:single SEED=environment
bundle exec rake db:seed:single SEED=server
